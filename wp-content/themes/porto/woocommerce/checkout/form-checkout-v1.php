<?php
/**
 * Checkout Form V1
 *
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$porto_woo_version = porto_get_woo_version_number();
$checkout          = WC()->checkout();

// filter hook for include new pages inside the payment method
$get_checkout_url = version_compare( $porto_woo_version, '2.5', '<' ) ? apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ) : wc_get_checkout_url(); ?>
<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900,900i&amp;display=swap" rel="stylesheet">
<style type="text/css">
#yith-wcwl-popup-message {display:none!important;}
body {font-family: "Lato";}
		 span.selection ,input[type="text"] ,select , input[type="tel"] , input[type="email"]{
		 	border: 1px transparent solid;
			background-clip: padding-box;
			border-radius: 5px;
			display: block;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
			width: 100%;
			padding: 0.92857em 0.78571em;
			word-break: normal;
			line-height: inherit;
			background-color: white;
			color: #333333;
			border-color: #d9d9d9;
		};
.guaranteedays {
    font-size: 21px;
    font-weight: 900;
    opacity: .65;
    text-align: center;
    color: #434343;
    line-height: 24px;
    margin: 0 auto 18px;
    width: 308px;
    max-width: 100%;
}
div#main{background:#FAFAFA}
/*.payment_methods {display:none}*/



</style>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

<div class="row" id="">
	<div class="col-lg-6" id="">
			<div class="checkout-order-review featured-box featured-box-primary align-left" style="background-color: #F6F8F9;">
		<div class="box-content">
			<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'porto' ); ?></h3>

			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>

			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>
	</div>
	</div>
	<div class="col-lg-6" id="">
			<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="row" id="customer_details">
			<div class="col-lg-12">
				<div class="featured-box featured-box-primary align-left">
					<div class="box-content">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					</div>
				</div>
			</div>

	<!-- 		<div class="col-lg-6">
				<div class="featured-box featured-box-primary align-left">
					<div class="box-content">
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
					</div>
				</div>
			</div> -->
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	</div>
</div>





	<div class="row">
			<div class="col-lg-6">
				<div class="featured-box featured-box-primary align-left" style="background-color: #F6F8F9;">
					<div style="border-top-color: #00c972;border-bottom-color: #9CC146" class="box-content" id="moved-payment-layout">
						<div class="row">
							<div style="margin: 0 auto" >
	<!--						<img   class="img-responsive footer-payment-img" src="https://i.pinimg.com/originals/ab/ea/c2/abeac21d910701867cd78a6e5a72fe07.png" width="200" alt="Payment Gateways">	-->
							<img   class="strp-img-resp img-responsive footer-payment-img" src="/wp-content/uploads/stripe_secure.png" width="200" alt="Payment Gateways">
							</div>
								
						<div>
					</div>
				</div>
		</div>
	
	<div class="box-content" style="border-top-color:#00c972">
		<div style="margin:10px">
		<p class="guarantee-btn">
<object data="/wp-content/uploads/history.svg" type="image/svg+xml">
  <img src="/wp-content/uploads/history.png" width="13" height="13" alt="History Image" class="history-img">
</object>
You're 100% backed by our 30-day money-back guarantee.</p>
		</div>
		<div style="text-align:center;margin:10px">
				  <img src="/wp-content/uploads/guarrantee.png" width="97" height="97" alt="Guarrantee Image" class="guarantee-img">
		</div>
		<div>
			<p class="guaranteedays" style="font-size:16px;
    font-weight: 900;
    opacity: .65;
    text-align: center;
    color: #434343;
    line-height: 24px;
    margin: 0 auto 18px;
    width: 308px;
    max-width: 100%;">30 Days Money Back Guarantee</p>
		</div>
	<div>
	<p class="althoughwedont" style="opacity: 0.65;
    font-family: Lato;
    font-size: 15px;
    text-align: center;
    color: #434343;
    width: 308px;
    margin: 0 auto 41px;
    max-width: 100%;">Although we don’t think you’ll ever want one, we’ll gladly provide
a
refund if it’s requested within 30 days of purchase.</p>
	</div>
	<div style="text-align:center;margin:10px">
			  <img src="/wp-content/uploads/northon.png" width="87" height="44" alt="norton" class="norton">
	</div>
	<div>
		<p class="allrights" style="text-align: center;
    font-size: 16px;
    line-height: 1.44;
    letter-spacing: -0.32px;
    color: #2a2a2a;
    padding-bottom: 46px;
    line-height: 32px;
    max-width: 295px;
    margin: auto;">All Rights Reserved, 2020 - Terms of Service - Made with 
  <img src="/wp-content/uploads/heart.png" width="17" height="15" alt="Heart Image" class="heart-img">
and 
  <img src="/wp-content/uploads/coffee.png" width="19" height="14" alt="Coffee Image" class="coffee-img">
</p>
	<div>
 	 </div>
	</div>


</form>
