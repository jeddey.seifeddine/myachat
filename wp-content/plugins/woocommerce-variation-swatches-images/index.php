<?php
/*
  Plugin Name: WooCommerce Variation Swatches Images
  Plugin URI: https://codecanyon.net/user/ma-group/portfolio
  Description: WooCommerce Swatches allows you to configure images for product variations
  Version: 1.0.5
  Author: ma_group
  Author URI: https://codecanyon.net/user/ma-group
  Text Domain: wcvs_swatches
  Domain Path: /languages
 */

if (!defined('WCVS_ROOT_PATH')) {
    define('WCVS_ROOT_PATH', plugin_dir_path(__FILE__));
}

if (!defined('WCVS_ROOT_URL')) {
    define('WCVS_ROOT_URL', plugin_dir_url(__FILE__));
}

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (!defined('WCVS_SWATCHES_VERSION')) {
    $plugin_data = get_plugin_data(__FILE__);
    define('WCVS_SWATCHES_VERSION', $plugin_data['Version']);
}

include_once(dirname(__FILE__) . '/includes/init.php');
WCVS_Init::init(WCVS_ROOT_PATH . 'includes/classes');

if (!class_exists('WCVS_SwatchesMain')) {

    class WCVS_SwatchesMain {

        function __construct() {
            register_activation_hook(__FILE__, array($this, 'install'));
            register_deactivation_hook(__FILE__, array($this, 'uninstall'));

            if (WCVS_WooCommerce::woocommerce_active_check()) {
                add_action('init', array($this, 'load_textdomain'), 0);
                add_action('plugins_loaded', array($this, 'on_plugin_loaded'));
            }
        }

        function install() {
            do_action("wcvs_install");
        }

        function uninstall() {
            do_action("wcvs_uninstall");
        }

        function load_textdomain() {
            load_plugin_textdomain('wcvs_swatches', false, plugin_basename(dirname(__FILE__)) . '/languages');
        }

        function on_plugin_loaded() {
            if (WCVS_Swatches_Compatibility::is_wc_version_gte_2_7()) {
                $GLOBALS['wcvs_swatches'] = new WCVS_SwatchesPlugin();
            } else {
                //not suported yet
            }
        }

    }

}

new WCVS_SwatchesMain();