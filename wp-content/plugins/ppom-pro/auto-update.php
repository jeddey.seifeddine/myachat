<?php
/**
 * Auto Update for New version
 * */
 

define("PPOM_PRO_CHANGELOG_JSON", 'https://nm-plugins.s3.amazonaws.com/'.PPOM_PLUGIN_ID.'/plugin-info.json');
define("PPOM_PRO_TRANSIENT_KEY", "plugin_update_transient_".PPOM_PLUGIN_ID);
define("PPOM_PRO_SLUG", 'ppom-pro');
define("PPOM_PRO_FILENAME", 'ppom.php');

add_filter('plugins_api', 'ppom_pro_plugin_info', PPOM_PLUGIN_ID, 3);
/*
 * $res empty at this step
 * $action 'plugin_information'
 * $args stdClass Object ( [slug] => woocommerce [is_ssl] => [fields] => Array ( [banners] => 1 [reviews] => 1 [downloaded] => [active_installs] => 1 ) [per_page] => 24 [locale] => en_US )
 */
function ppom_pro_plugin_info( $res, $action, $args ){
	
	if( ! NM_AUTH(PPOM_PLUGIN_PATH, PPOM_REDIRECT_URL, PPOM_PLUGIN_ID) -> api_key_found() ) 
		return $res;
	

	// do nothing if this is not about getting plugin information
	if( $action !== 'plugin_information' )
		return $res;

	// do nothing if it is not our plugin
	if( 'ppom-pro' !== $args->slug )
		return $res;
		
	// trying to get from cache first
	if( false == $remote = get_transient( PPOM_PRO_TRANSIENT_KEY ) ) {

		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( PPOM_PRO_CHANGELOG_JSON, array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			) )
		);
		
// 		var_dump($remote);

		if ( !is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && !empty( $remote['body'] ) ) {
			set_transient( PPOM_PRO_TRANSIENT_KEY, $remote, 43200 ); // 12 hours cache
		}

	}
	
	

	if( !is_wp_error( $remote ) ) {
		
		$remote = json_decode( $remote['body'], FALSE);
		
// 	}
	
		$res = new stdClass();
		$res->name = $remote->name;
		$res->slug = 'ppom-pro';
		$res->version = $remote->version;
		$res->tested = $remote->tested;
		$res->requires = isset($remote->requires) ? $remote->requires : 0;
		$res->author = '<a href="https://najeebmedia.com.com">N-Media</a>';
		$res->author_profile = 'https://profiles.wordpress.org/nmedia82';
		$res->download_link = $remote->download_url;
		$res->trunk = $remote->download_url;
		$res->last_updated = isset($remote->last_updated) ? $remote->last_updated : 0;
		$res->sections = array(
			'description' =>nl2br($remote->sections->description),
			//'installation' => $remote->sections->installation,
			'changelog'  => nl2br($remote->sections->description),
			// you can add your custom sections (tabs) here
		);
		if( !empty( $remote->sections->screenshots ) ) {
			$res->sections['screenshots'] = $remote->sections->screenshots;
		}

		$res->banners = array(
			'low' => $remote->banners->low,
            // 'high' => 'https://YOUR_WEBSITE/banner-1544x500.jpg'
		);
    // ppom_pa($res);
          	return $res;

	}

	return false;

}
add_filter('site_transient_update_plugins', 'ppom_pro_push_update' );

function ppom_pro_push_update( $transient ){

    // delete_transient( PPOM_PRO_TRANSIENT_KEY );
	if ( empty($transient->checked ) ) {
            return $transient;
        }

	// trying to get from cache first
	if( false == $remote = get_transient( PPOM_PRO_TRANSIENT_KEY ) ) {

		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( PPOM_PRO_CHANGELOG_JSON, array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			) )
		);

		if ( !is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && !empty( $remote['body'] ) ) {
			set_transient( PPOM_PRO_TRANSIENT_KEY, $remote, 43200 ); // 12 hours cache
		}

	}

	if( !is_wp_error( $remote ) ) {

		$remote = json_decode( $remote['body'] );
		$req = isset($remote->requires) ? $remote->requires : 0;
		if( $remote && version_compare( PPOM_PRO_VERSION, $remote->version, '<' )
			&& version_compare($req, get_bloginfo('version'), '<' ) ) {
				$res = new stdClass();
				$res->id = 'nm-plugin-'.PPOM_PLUGIN_ID;
				$res->slug = PPOM_PRO_SLUG;
				$res->plugin = PPOM_PRO_FILENAME;
				$res->new_version = $remote->version;
				$res->tested = $remote->tested;
				$res->package = $remote->download_url;
				$res->compatibility = new stdClass();
				$transient_key = $res->slug.'/'.$res->plugin;
           		$transient->response[$transient_key] = $res;
           		//$transient->checked[$res->plugin] = $remote->version;
           	}

	}
    // ppom_pa($transient); exit;
    return $transient;
}

add_action( 'upgrader_process_complete', 'ppom_pro_after_update', 10, 2 );

function ppom_pro_after_update( $upgrader_object, $options ) {
	if ( $options['action'] == 'update' && $options['type'] === 'plugin' )  {
		delete_transient( PPOM_PRO_TRANSIENT_KEY );
	}
}