/* global jQuery, ajaxurl, wdr_data */
jQuery(document).ready(function ($) {
    const alert_counter = {counts: 1};

    /**
     * Filter Block
     */
    $('.wdr-btn-add-product-filter').click(function () {
        wdr_buildrule.show_hide_rule_block({
            showBlockId: ".wdr-filter-block",
            hideBlockId: '.wdr-discount-template, .wdr-advanced-layout-block',
            thisObject: this,
        });
    });

    /*Add filter section*/
    $('.add-product-filter').click(function () {
        var last_index = $('.wdr-filter-group-items').children().last().attr('data-index');
        last_index = CalculateDataIndex(last_index);

        wdr_buildrule.wdr_clone_field({
            addFilterType: '.wdr-build-filter-type',
            addFilterMethod: '.products',
            addRemoveIcon: '.wdr-icon-remove',
            ruleAppendTo: ".wdr-filter-group-items",
            newIndex: last_index
        });
        make_wdr_select2_search($('.wdr-filter-group[data-index="' + last_index + '"]').find('[data-field="autocomplete"]'));
        $('.wdr-filter-group[data-index=' + last_index + ']').append("<div class='wdr_filter_desc_text'>" + wdr_data.localization_data.filter_products + "</div>");
    });

    /*Remove filter section*/
    $(document).on('click', '.remove-current-row', function () {

        if ($('.wdr-filter-group-items > div').length >= 2) {
            wdr_buildrule.remove_wdr_field_group({
                parentsRow: ".wdr-filter-group",
                thisObject: this,
            });
        }
    });

    /*Add filter section while change select option*/
    $(document).on('change', '.wdr-product-filter-type', function () {
        let last_index = $(this).parents('.wdr-filter-group').data('index');
        let current_block = $(this).val();
        wdr_buildrule.remove_wdr_field_group({
            parentRow: $(this).parent(),
        });
        wdr_buildrule.wdr_clone_field({
            addFilterMethod: '.' + current_block,
            addRemoveIcon: '.wdr-icon-remove',
            ruleAppendTo: $(this).parents('.wdr-filter-group'),
            newIndex: last_index
        });
        //
        switch(current_block) {
            case "products":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_products + '</div>');
                break;
            case "product_category":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_Category + '</div>');
                break;
            case "product_attributes":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_Attributes + '</div>');
                break;
            case "product_tags":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_Tags + '</div>');
                break;
            case "product_sku":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_SKUs + '</div>');
                break;
            case "product_on_sale":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_On_sale_products + '</div>');
                break;
            case "all_products":
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_all_products + '</div>');
                break;
            default:
                $('.wdr-filter-group[data-index="'+ last_index +'"]').append('<div class="wdr_filter_desc_text">' + wdr_data.localization_data.filter_custom_taxonomies + '</div>');
                break;

        }
        make_wdr_select2_search($(this).parents('.wdr-filter-group').find('[data-field="autocomplete"]'));
    });


    /**
     * Condition Block
     */
    $('.wdr-btn-add-condition').click(function () {
        wdr_buildrule.show_hide_rule_block({
            showBlockId: ".wdr-condition-template",
            hideBlockId: ".wdr-filter-block, .wdr-discount-template, .wdr-advanced-layout-block",
            thisObject: this,
        });
    });

    /*Add Discount section*/
    $('.add-product-condition').click(function () {
        var last_index = $('.wdr-condition-group-items').children().last().attr('data-index');
        last_index = CalculateDataIndex(last_index);
        wdr_buildrule.wdr_clone_field({
            addConditionType: '.wdr-build-condition-type',
            addFilterMethod: '.cart_subtotal',
            addRemoveIcon: '.wdr-icon-remove',
            ruleAppendTo: ".wdr-condition-group-items",
            newIndex: last_index
        });
    });

    /*Remove section*/
    $(document).on('click', '.remove-current-row', function () {
        if ($('.wdr-condition-group-items > div').length >= 2) {
            wdr_buildrule.remove_wdr_field_group({
                parentsRow: ".wdr-conditions-container",
                thisObject: this,
            });
        }
    });

    /*Add condition section on select option*/
    $(document).on('change', '.wdr-product-condition-type', function () {
        var last_index = $(this).parents('.wdr-condition-group').data('index');
        var current_block = $(this).val();

        wdr_buildrule.remove_wdr_field_group({
            parentRow: $(this).parent()
        });
        wdr_buildrule.wdr_clone_field({
            addFilterMethod: '.' + current_block,
            addRemoveIcon: '.wdr-icon-remove',
            ruleAppendTo: $(this).parents('.wdr-conditions-container'),
            newIndex: last_index
        });
        if (current_block == 'order_time') {
            $('.wdr_time_picker').datetimepicker({
                datepicker: false,
                format: 'H:i'
            });
        }
        //$('.wdr-condition-date').datetimepicker();
        make_wdr_select2_search($(this).parents('.wdr-conditions-container').find('[data-field="autocomplete"]'));
        make_select2_preloaded($(this).parents('.wdr-conditions-container').find('[data-field="preloaded"]'));
        make_select2_all_loaded($(this).parents('.wdr-conditions-container').find('[data-field="autoloaded"]'));
        wdr_initialize_datetime($(this).parents('.wdr-conditions-container').find('[data-field="date"]'));
    });

    /*Initialise and show & hide coupon search section*/
    $(document).on('change', '.wdr_copon_type', function () {
        var coupon_type = $(this).val();

        if (coupon_type === "at_least_one_any") {
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-search').css("display", "none");
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-value').css("display", "none");
            //$(this).parents('.wdr_cart_coupon_group').find('#rm-coupon option:selected').remove();
        } else if (coupon_type === "none_at_all") {
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-search').css("display", "none");
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-value').css("display", "none");
            //$(this).parents('.wdr_cart_coupon_group').find('#rm-coupon option:selected').remove();
        } else if (coupon_type === "custom_coupon") {
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-search').css("display", "none");
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-value').css("display", "block");
            //$(this).parents('.wdr_cart_coupon_group').find('#rm-coupon option:selected').remove();
        } else {
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-value').css("display", "none");
            $(this).parents('.wdr_cart_coupon_group').find('.wdr-cart-coupon-search').css("display", "block");
            make_wdr_select2_search($(this).parents('.wdr-conditions-container').find('[data-field="autocomplete"]'));
        }
    });

    /**
     * Discount Block
     */
    $('.wdr-btn-add-discount').click(function () {
        wdr_buildrule.show_hide_rule_block({
            showBlockId: ".wdr-discount-template",
            hideBlockId: '.wdr-filter-block, .wdr-advanced-layout-block',
            thisObject: this,
        });
    });

    /*Add section*/
    $(document).on('click', '.add_discount_elements', function () {
        //alert(1);
        var data_append = $(this).data('append');
        var discount_ele = $(this).data('discount-method');
        var next_starting_value = $(this).data('next-starting-value');
        var last_index = $('.' + data_append).children().last().attr('data-index');
        last_index = CalculateDataIndex(last_index);
        wdr_buildrule.wdr_clone_field({
            addFilterMethod: '.' + discount_ele,
            ruleAppendTo: '.' + data_append,
            addDiscountElement: 'enable',
            newIndex: last_index
        });

        if (next_starting_value != '' && next_starting_value != 'undefined') {
            $('.' + data_append + ' ' + next_starting_value + ':last-child').find('.awdr_value_selector').val('');
            let bogo_auto_add_value = $('.' + data_append + ' ' + next_starting_value + ':nth-last-child(2)').find('.awdr_auto_add_value').val();
            bogo_auto_add_value = parseInt(bogo_auto_add_value) + 1;
            if (bogo_auto_add_value != '' && !isNaN(bogo_auto_add_value)) {
                $('.' + data_append + ' ' + next_starting_value + ':nth-last-child(1)').find('.awdr_next_value').val(bogo_auto_add_value);
            }
        }
        if (discount_ele == 'add_buyx_gety_range') {
            $('.buyx_gety_individual_range[data-index=' + last_index + ']').find('.awdr-buyx-gety-max').show();
            $('.buyx_gety_individual_range[data-index=' + last_index + ']').find('.awdr-bogo-recurcive').prop("checked", false);
            $('.wdr-buyx-gety-discount-inner').css("border-bottom", "1px solid #ddd");
        } else if (discount_ele == 'add_buyx_getx_range') {
            $('.buyx_getx_individual_range[data-index=' + last_index + ']').find('.awdr-buyx-getx-max').show();
            $('.buyx_getx_individual_range[data-index=' + last_index + ']').find('.awdr-bogo-recurcive').prop("checked", false);
            $('.buyx_getx_individual_range').css("border-bottom", "1px solid #ddd");
        }
        make_wdr_select2_search($('.' + data_append + ' div:last-child').find('[data-field="autocomplete"]'));
        make_wdr_select2_search($('.' + data_append + ' div:last-child').find('[data-list="product_category"]'));
    });

    /*Remove Section*/
    $(document).on('click', '.wdr_discount_remove', function () {
        wdr_buildrule.remove_wdr_field_group({
            parentsRow: '.wdr-discount-group',
            thisObject: this,
        });
    });

    /*Discounts Tabs navigation*/
    $(document).on('click', '[data-click="wdr-bottombar"]', function () {
        var show_discount_content = $(this).data('dtype');
        $(this).hide();
        $('.' + show_discount_content).show();
    });

    /*Hide input for Free shipping value*/
    $(document).on('change', '.cart_free_shipping', function () {
        var if_free_shipping = $(this).val();
        if (if_free_shipping == 'free_shipping') {
            $(this).parent().siblings('.cart_discount_value').hide();
        } else {
            $(this).parent().siblings('.cart_discount_value').show();
        }
    });

    function awdr_process_v1_to_v2_migration(){
        $.ajax({
            data: {method: 'do_v1_v2_migration', action: 'wdr_ajax'},
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if(data.data.status != undefined){
                    if(data.data.status == "completed"){
                        notify(wdr_data.localization_data.processing_migration_success_message, 'success', alert_counter);
                        location.reload();
                    } else {
                        awdr_process_v1_to_v2_migration();
                    }
                    $(".wdr_migration_process_status").html(data.data.display_text);
                } else {
                    location.reload();
                }
            }
        });
    }

    /**
     * Process on sale list
     */
    $(document).on('click', '#awdr_rebuild_on_sale_list', function () {
        var rules = $("#awdr_rebuild_on_sale_rules").val();
        $(".awdr_rebuild_on_sale_list_notice").html("");
        if(rules != null){
            $("#awdr_rebuild_on_sale_list").attr('disabled', "disabled");
            $("#awdr_rebuild_on_sale_list").html(wdr_data.localization_data.rebuild_on_sale_list_processing_text);
            awdr_process_on_sale_list(rules, $(this));
        } else {
            $(".awdr_rebuild_on_sale_list_notice").html(wdr_data.localization_data.rebuild_on_sale_list_error_please_select_rule);
        }
    });

    /**
     * Process on sale list
     */
    $(document).on('click', '#awdr_rebuild_on_sale_list_on_rule_page', function () {
        $(this).attr('disabled', "disabled");
        $(this).html(wdr_data.localization_data.rebuild_on_sale_list_processing_text);
        awdr_process_on_sale_list(null, $(this));
        //$(".awdr_rebuild_on_sale_rule_page_con").removeClass("need_attention");
    });

    function awdr_process_on_sale_list(rules, current_obj){
        $.ajax({
            data: {method: 'rebuild_onsale_list', action: 'wdr_ajax', rules: rules},
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
                current_obj.removeAttr('disabled');
            },
            success: function (data) {
                current_obj.html(wdr_data.localization_data.rebuild_on_sale_list_processed_text);
                current_obj.removeAttr('disabled');
            }
        });
    }

    /**
     * Process migration
     */
    $(document).on('click', '#awdr_do_v1_v2_migration', function () {
        $(".wdr_migration_process").append(wdr_data.localization_data.processing_migration_text);
        awdr_process_v1_to_v2_migration();
    });

    /**
     * Skip migration
     */
    $(document).on('click', '#awdr_skip_v1_v2_migration', function () {
        $(".wdr_migration_process").append(wdr_data.localization_data.skip_migration_text);
        $.ajax({
            data: {method: 'skip_v1_v2_migration', action: 'wdr_ajax'},
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if (data === 'failed') {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                } else if(data.data === true){
                    $(".wdr_migration_process").append(wdr_data.localization_data.skip_migration_success_message);
                    notify(wdr_data.localization_data.skip_migration_success_message, 'success', alert_counter);
                }
                location.reload();
            }
        });
    });

    $('.awdr-switch-version-button').on('click', function (event) {
        event.preventDefault();
        var version = $(this).attr('data-version');
        var page = $(this).attr('data-page');
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {action: 'awdr_switch_version', version: version, page: page},
            success: function (data) {
                if(data.data.status == true){
                    window.location.replace(data.data.url);
                }
                $(".wdr_switch_message").html(data.data.message);
            }
        });
    });

    /**
     * Duplicate Rule
     */
    $(document).on('click', '.wdr_duplicate_rule', function () {
        $.ajax({
            data: {rowid: $(this).data('duplicate-rule'), method: 'duplicate_rule', action: 'wdr_ajax'},
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if (data === 'failed') {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                } else {
                    notify(wdr_data.localization_data.duplicate_rule, 'success', alert_counter);
                }
                location.reload();
            }
        });
    });

    /**
     * Delete Rule
     */
    $(document).on('click', '.wdr_delete_rule', function () {
        var wdr_delete_rule_row = $(this).closest('tr');
        if (confirm(wdr_data.localization_data.delete_confirm)) {
            $.ajax({
                data: {rowid: $(this).data('delete-rule'), method: 'delete_rule', action: 'wdr_ajax'},
                type: 'post',
                url: ajaxurl,
                error: function (request, error) {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                },
                success: function (data) {
                    if (data === 'failed') {
                        notify(wdr_data.localization_data.error, 'error', alert_counter);
                    } else {
                        notify(wdr_data.localization_data.deleted_rule, 'success', alert_counter);
                        wdr_delete_rule_row.hide(500, function () {
                            wdr_delete_rule_row.remove();
                        });
                    }
                }
            });
        }
    });

    /**
     * Manage Rule Status
     */
    $(document).on('change', '.wdr_manage_status', function () {
        let change_status = '';
        let parent_tr = $(this).closest('tr');
        if ($(this).prop("checked") == true) {
            change_status = 1;
        } else {
            change_status = 0;
        }
        $.ajax({
            data: {
                rowid: $(this).data('manage-status'),
                method: 'manage_status',
                action: 'wdr_ajax',
                changeto: change_status
            },
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if (data === 'failed') {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                } else {
                    if (change_status == 1) {
                        $(parent_tr).find('.awdr-enabled-status').show();
                        notify(wdr_data.localization_data.enabled_rule, 'success', alert_counter);
                    } else {
                        $(parent_tr).find('.awdr-enabled-status').hide();
                        notify(wdr_data.localization_data.disabled_rule, 'success', alert_counter);
                    }
                }
            }
        });
    });

    /**
     * ajax search function
     * @param $el
     */
    function make_wdr_select2_search($el) {
        $el.select2({
            width: '100%',
            minimumInputLength: 1,
            placeholder: $el.data('placeholder'),
            escapeMarkup: function (text) {
                return text;
            },
            language: {
                noResults: function () {
                    return wdr_data.labels.select2_no_results;
                }
            },
            ajax: {
                url: ajaxurl,
                type: 'POST',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term,
                        action: 'wdr_ajax',
                        method: $el.data('list') || 'products',
                        taxonomy: $el.data('taxonomy') || '',
                        selected: $el.val()
                    };
                },
                processResults: function (response) {
                    return {results: response.data || []};
                }
            }
        });
        $el.parent().find('.select2-search__field').css('width', '100%');
    }

    /**
     * ajax edit search function on document ready
     */
    $('.edit-filters').select2({
        width: '100%',
        minimumInputLength: 1,
        placeholder: wdr_data.labels.placeholders,
        language: {
            noResults: function () {
                return wdr_data.labels.select2_no_results;
            }
        },
        ajax: {
            url: ajaxurl,
            type: 'POST',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    query: params.term,
                    action: 'wdr_ajax',
                    method: $(this).data('list') || 'products',
                    taxonomy: $(this).data('taxonomy') || '',
                    selected: $(this).val()
                };
            },
            processResults: function (response) {
                return {results: response.data || []};
            }
        }
    });
    $('.edit-filters').parent().find('.select2-search__field').css('width', '100%');


    /**
     * Preloaded values search function
     * @param $el
     */
    function make_select2_preloaded($els) {
        $els.each(function (index, el) {
            var $el = $(el);
            var data = wdr_data.lists[$el.data('list')];
            $el.select2({
                width: '100%',
                escapeMarkup: function (text) {
                    return text;
                },
                minimumInputLength: 1,
                placeholder: $el.data('placeholder'),
                language: {
                    noResults: function () {
                        return wdr_data.labels.select2_no_results;
                    }
                },
                data: data
            });

            $el.parent().find('.select2-search__field').css('width', '100%');
        });
    }

    /**
     * Preloaded values search function
     * @param $el
     */
    function make_select2_all_loaded($els) {
        $els.each(function (index, el) {
            var $el = $(el);
            var data = wdr_data.lists[$el.data('list')];
            $el.select2({
                width: '100%',
                escapeMarkup: function (text) {
                    return text;
                },
                placeholder: $el.data('placeholder'),
                language: {
                    noResults: function () {
                        return wdr_data.labels.select2_no_results;
                    }
                },
                data: data
            });

            $el.parent().find('.select2-search__field').css('width', '100%');
        });
    }

    /**
     * ajax edit pre_loaded search function in document ready
     */
    $('.edit-preloaded-values').each(function (index, el) {
        var $el = $(el);
        var data = wdr_data.lists[$el.data('list')];

        $el.select2({
            width: '100%',
            escapeMarkup: function (text) {
                return text;
            },
            minimumInputLength: 1,
            placeholder: $el.data('placeholder'),
            language: {
                noResults: function () {
                    return wdr_data.labels.select2_no_results;
                }
            },
            data: data
        });

        $el.parent().find('.select2-search__field').css('width', '100%');
    });

    /**
     * ajax edit pre_loaded search function in document ready
     */
    $('.edit-all-loaded-values').each(function (index, el) {
        var $el = $(el);
        var data = wdr_data.lists[$el.data('list')];

        $el.select2({
            width: '100%',
            escapeMarkup: function (text) {
                return text;
            },
            placeholder: $el.data('placeholder'),
            language: {
                noResults: function () {
                    return wdr_data.labels.select2_no_results;
                }
            },
            data: data
        });

        $el.parent().find('.select2-search__field').css('width', '100%');
    });

    /**
     * Date Time picker Initialize for on change
     * @param $els
     */
    function wdr_initialize_datetime($els) {
        $els.each(function (index, el) {
            var $el = $(el);
            var datepicker_type = $el.data('class');

            if (datepicker_type == 'start_dateonly') {
                $('[data-class="' + datepicker_type + '"]').datetimepicker({
                    format: 'Y-m-d',
                    onShow: function (ct) {
                        this.setOptions({
                            maxDate: $('[data-class="end_dateonly"]').val() ? $('[data-class="end_dateonly"]').val() : false
                        })
                    },
                    timepicker: false,
                });
            } else if (datepicker_type == 'end_dateonly') {
                $('[data-class="' + datepicker_type + '"]').datetimepicker({
                    format: 'Y-m-d',
                    onShow: function (ct) {
                        this.setOptions({
                            minDate: $('[data-class="start_dateonly"]').val() ? $('[data-class="start_dateonly"]').val() : false
                        })
                    },
                    timepicker: false,
                });
            } else if (datepicker_type == 'start_datetimeonly') {
                $('[data-class="' + datepicker_type + '"]').datetimepicker({
                    timepicker: true,
                    format: 'Y-m-d H:i',
                    onShow: function (ct) {
                        this.setOptions({
                            maxDate: $('[data-class="end_datetimeonly"]').val() ? $('[data-class="end_datetimeonly"]').val() : false
                        })
                    },
                });
            } else if (datepicker_type == 'end_datetimeonly') {
                $('[data-class="' + datepicker_type + '"]').datetimepicker({
                    timepicker: true,
                    format: 'Y-m-d H:i',
                    onShow: function (ct) {
                        this.setOptions({
                            minDate: $('[data-class="start_datetimeonly"]').val() ? $('[data-class="start_datetimeonly"]').val() : false
                        })
                    },
                });
            }

        });
    }

    /**
     * Calculate Data Index Value
     * @param last_index
     * @returns {number}
     * @constructor
     */
    function CalculateDataIndex(last_index) {
        if (last_index === 0) {
            return 0;
        } else {
            return parseInt(last_index) + 1;
        }
    }

    /**
     * Save Rule using ajax
     */
    $('#wdr-save-rule').submit(function (e) {
        e.preventDefault();
        let loader = $('.woo_discount_loader');
        $.ajax({
            data: $(this).serialize(),
            type: 'post',
            url: ajaxurl,
            beforeSend: function() {
                loader.show();
            },
            complete: function() {
                loader.hide();
            },
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if(data.build_index.required_rebuild != undefined){
                    if(data.build_index.required_rebuild == true){
                        $(".awdr_rebuild_on_sale_rule_page_con").addClass("need_attention");
                    }
                }
                if (data.redirect) {
                    window.location.href = data.redirect;
                    notify(wdr_data.localization_data.save_rule, 'success', alert_counter);
                } else if (data.coupon_message) {
                    $(".wdr-btn-add-condition").trigger("click");
                    $(".coupon_name_msg").css("border", "1px solid #FF0000").focus();
                    notify(wdr_data.localization_data.coupon_exists, 'error', alert_counter);
                } else {
                    $('.wdr_desc_text.coupon_error_msg').hide();
                    $(".coupon_name_msg").css("border", "");
                    notify(wdr_data.localization_data.save_rule, 'success', alert_counter);
                }

            }
        });
    });

    /**
     * Save and Close Button
     */
    $(document).on('click', '.wdr_save_close', function () {
        $('input[name=wdr_save_close]').val('1');
        $(".wdr_save_stay").click();
    });

    /**
     * save default configuration settings
     */
    $('#configuration-form').submit(function (e) {
        e.preventDefault();
        $("#awdr_banner_editor-html").click();
        $("#awdr_banner_editor-tmce").click();
        let awdr_banner_editer = $('#awdr_banner_editor').val();
        let values = $(this).serialize();
        //let loader = $('.woo_discount_loader');
        values += "&banner_content=" + awdr_banner_editer;
        $.ajax({
            data: values,
            type: 'post',
            url: ajaxurl,
            beforeSend: function() {
                //loader.show();
            },
            complete: function() {
                //loader.hide();
            },
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (response) {
                if (response.data.save_popup == "alert_in_popup") {
                    $('.awdr-save-green').show();
                    setTimeout(
                        function () {
                            $('.awdr-save-green').fadeOut(500);
                        }, 2500
                    );
                } else if (response.data.save_popup == "alert_in_normal") {
                    notify(wdr_data.localization_data.save_settings, 'success', alert_counter);
                }
            }
        });
    });

    /**
     * Save and Close settings
     */
    $(document).on('click', '.bulk-table-customized-setting', function () {
        $('input[name=customizer_save_alert]').val('1');
        $(".save-configuration-submit").click();
    });

    $('#sort_customizable_table').dragtable({
        persistState: function (table) {
            table.el.find('th').each(function (i) {
                if (this.id != '') {
                    table.sortOrder[this.id] = i;
                    if (this.id == 'customize-bulk-table-title') {
                        $('.customize_bulk_table_title').val(i);
                    } else if (this.id == 'customize-bulk-table-discount') {
                        $('.customize_bulk_table_discount').val(i);
                    } else if (this.id == 'customize-bulk-table-range') {
                        $('.customize_bulk_table_range').val(i);
                    }
                }
            });
        }
    });


    /**
     * Display page bulk action on top
     */
    $('#wdr-bulk-action-top').submit(function (e) {
        e.preventDefault();
        if($('input[name="saved_rules[]"]:checked').length > 0) {
            let action = $('#bulk-action-selector-top').val();
            let result;
            if (action == 'enable') {
                result = confirm("Are you sure to enable the selected rules?");
            } else if (action == 'disable') {
                result = confirm("Are you sure to disable the selected rules?");
            } else if (action == 'delete') {
                result = confirm("Are you sure to delete the selected rules?");
            } else {
                return false;
            }
            if (result == false) {
                return false;
            }
        }else{
            return false;
        }
        $.ajax({
            data: $(this).serialize(),
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if (data.disable == 'disabled') {
                    notify(wdr_data.localization_data.disabled_rule, 'success', alert_counter);
                } else if (data.enable == 'enabled') {
                    notify(wdr_data.localization_data.enabled_rule, 'success', alert_counter);
                } else if (data.delete == 'deleted') {
                    notify(wdr_data.localization_data.deleted_rule, 'success', alert_counter);
                } else {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                }
                window.location.replace(wdr_data.admin_url);
            }
        });
    });

    /**
     * Display page search action on top
     */
    $('#wdr-search-top').submit(function (e) {
        e.preventDefault();
        var search_value = $(this).serializeArray();
        var adminUrl = search_value[0].value;
        var searchQry = search_value[1].value;
        var redirectUrl = adminUrl + '&name=' + searchQry;
        window.location.replace(redirectUrl);
    });

    /**
     * Number Validation
     * float only
     */
    $(document).on("keypress keyup blur", ".float_only_field", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which !== 46 || $(this).val().indexOf('.') !== -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    /**
     * Number only
     */
    $(document).on("keypress keyup blur", ".number_only_field", function (event) {
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    /**
     * Initialize date pickers in document ready
     */
    $('[data-class="start_dateonly"]').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('[data-class="end_dateonly"]').val() ? $('[data-class="end_dateonly"]').val() : false
            })
        },
    });
    $('[data-class="end_dateonly"]').datetimepicker({
        timepicker: false,
        format: 'Y-m-d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('[data-class="start_dateonly"]').val() ? $('[data-class="start_dateonly"]').val() : false
            })
        },
    });

    $('[data-class="start_datetimeonly"]').datetimepicker({
        timepicker: true,
        format: 'Y-m-d H:i',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('[data-class="end_datetimeonly"]').val() ? $('[data-class="end_datetimeonly"]').val() : false
            })
        },
    });
    $('[data-class="end_datetimeonly"]').datetimepicker({
        timepicker: true,
        format: 'Y-m-d H:i',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('[data-class="start_datetimeonly"]').val() ? $('[data-class="start_datetimeonly"]').val() : false
            })
        },
    });

    $('.wdr_time_picker').datetimepicker({
        datepicker: false,
        format: 'H:i'
    });

    /**
     * bulk discount
     * hide and show the selected category
     */
    $(document).on('change', '.wdr-bulk-type', function () {

        var bulk_type = $(this).val();
        if (bulk_type == 'product_selected_categories') {
            $('.wdr-bulk-cat-selector').show();
        } else {
            // $('.wdr-bulk-cat-selector').hide().find('#rm-category option:selected').remove();
            $('.wdr-bulk-cat-selector').hide();
        }
    });

    /**
     * alert notification
     * @param message
     * @param type
     * @param alert_counter
     */
    function notify(message, type = "success", alert_counter = null) {

        switch (type) {
            case "error":
                var class_name = "wdr-alert-error";
                break;
            case "warning":
                var class_name = "wdr-alert-warning";
                break;
            default:
            case "success":
                var class_name = "wdr-alert-success";
                break;
        }

        let div_id = 'wdr-notify-msg-' + alert_counter.counts;
        let html = '<div style="display: none;" class="wdr-alert ' + class_name + '" id="' + div_id + '">' + message + '</div>';
        let notify_holder = $("#notify-msg-holder");
        notify_holder.append(html);
        let message_div = $("#" + div_id);
        var notify_count = alert_counter.counts;
        alert_counter.counts = parseInt(notify_count) + parseInt(1);
        message_div.fadeIn(500);
        setTimeout(
            function () {
                message_div.fadeOut(500);
                message_div.remove();
            }, 2500
        );
    }

    /**
     * settings
     * settings show hide option
     */
    $('.settings_option_show_hide').click(function () {
        var show_hide_class = $(this).data("name");
        if ($(this).val() == '1') {
            $('.' + show_hide_class).show();
            if (show_hide_class == 'hide_table_position') {
                $('.wdr-popup-link').show();
            }
        } else {
            $('.' + show_hide_class).hide();
            if (show_hide_class == 'hide_table_position') {
                $('.wdr-popup-link').hide();
            }
        }
    });

    /**
     * show hide bulk table options
     */
    $('.bulk_table_customizer_preview').click(function () {
        var col_name = $(this).data("colname");
        var show_hide = $(this).data("showhide");
        if (show_hide == 'show') {
            $('.' + col_name).show();
        } else {
            $('.' + col_name).hide();
        }
    });

    /**
     * settings
     * You saved text show hide option
     */
    $('.settings_option_show_hide_on_change').change(function () {

        if ($(this).val() == 'disabled') {
            $('.display_you_saved_text').hide();
        } else {
            $('.display_you_saved_text').show();
        }
    });

    /**
     * rules listing page
     * select and unselect the checkbox for bulk action
     */
    $(document).on('change', '.wdr-rules-select', function () {
        var bulk_selector = $(".wdr-rules-select");
        var selectable = $(this).val();
        if (selectable == 'off') {
            bulk_selector.val('on');
            $(".wdr-rules-selector").prop('checked', true);
        } else if (selectable == 'on') {
            bulk_selector.val('off');
            $(".wdr-rules-selector").prop('checked', false);

        }
    });
    /**
     * check all selected checkbox and check bulk action checkbox
     */
    $(document).on('change', '.wdr-rules-selector', function () {

        var bulk_selector = $(".wdr-rules-select");
        var totalCheckboxes = $('.wdr-ruleboard ' + 'input:checkbox').length;
        var numberOfChecked = $('.wdr-ruleboard ' + 'input:checkbox:checked').length;

        if (totalCheckboxes == numberOfChecked) {
            bulk_selector.val('on');
            bulk_selector.prop('checked', true);
        } else if (totalCheckboxes != numberOfChecked) {
            bulk_selector.val('off');
            bulk_selector.prop('checked', false);
        }
    });

    /**
     * Sticky Header
     */
    /* window.onscroll = function () {
         stickyHeader();
     };*/

    var header = document.getElementById("ruleHeader");

    function stickyHeader() {

        if (header != null) {
            var sticky = header.offsetTop;
            if (window.pageYOffset > sticky) {
                header.classList.add("wdr-sticky");
            } else {
                header.classList.remove("wdr-sticky");
            }
        }
    }

    /**
     * sorting rule priority
     */
    $("#sortable").sortable({
        delay: 150,
        stop: function (event, ui) {
            var selectedData = new Array();
            $('#sortable>tr').each(function () {
                selectedData.push($(this).attr("id"));
            });
            updatePriorityOrder(selectedData);
        }
    });

    function updatePriorityOrder(data) {
        $.ajax({
            data: {
                position: data,
                method: 'update_priority_order',
                action: 'wdr_ajax'
            },
            type: 'post',
            url: ajaxurl,
            error: function (request, error) {
                notify(wdr_data.localization_data.error, 'error', alert_counter);
            },
            success: function (data) {
                if (data === false) {
                    notify(wdr_data.localization_data.error, 'error', alert_counter);
                } else {
                    notify(wdr_data.localization_data.save_priority, 'success', alert_counter);
                }
            }
        });
    }

    $("#sortable").disableSelection();

    /**
     * read doc popup
     */
    $(document).on('click', '.help-popup', function () {
        var popup_id = '#' + $(this).attr('data-id');
        $(popup_id).dialog({
            modal: true,
            closeText: ''
        });
    });

    /**
     * Discount Buttons Show Hide
     */
    $(document).on('click', '.remove-clicked-discount-block', function () {
        var show_discount_button = $(this).data('showblock');
        var remove_value = $(this).data('removeval');
        var remove_option = $(this).data('removeopt');
        $('.' + show_discount_button).hide();
        $('.' + remove_value).removeAttr('value');
        $('.' + remove_option).prop('selectedIndex', 0);
        $('[data-dtype=' + show_discount_button + ']').show();

        if (show_discount_button == "wdr-bulk-discount") {
            $('.bulk_product_category_selector option:selected').remove();
            $('.wdr-bulk-cat-selector').hide();
        }
    });

    /**
     * Advanced layout tab
     */
    $('.wdr-btn-add-message').click(function () {
        wdr_buildrule.show_hide_rule_block({
            showBlockId: ".wdr-advanced-layout-block",
            hideBlockId: '.wdr-discount-template, .wdr-filter-block',
            thisObject: this,
        });
    });


    var bulk_min_range_length = $('.bulk-min').length;
    if (bulk_min_range_length >= 2) {
        var min_val = $('.bulk_discount_min').val();
        var max_val = $('.bulk_discount_max').val();
        var discount_val = $('.bulk_discount_value').val();
        if (min_val || max_val || discount_val) {
            $('.adv-msg-min-qty, .adv-msg-max-qty').show();
        }
    }

    var bulk_min_range_length = $('.set-min').length;
    if (bulk_min_range_length >= 2) {
        var min_val = $('.set_discount_min').val();
        var discount_val = $('.set_discount_value').val();
        if (min_val || discount_val) {
            $('.adv-msg-min-qty').show();
            $('.adv-msg-max-qty').hide();
        }
    }

    /**
     * settings
     * table discount column show hide option
     */
    $('.popup_table_discount_column_value').click(function () {

        if ($(this).val() == 1) {
            $('.wdr_table_discounted_value').show();
            $('.wdr_table_discounted_price').hide();
        } else {
            $('.wdr_table_discounted_value').hide();
            $('.wdr_table_discounted_price').show();
        }
    });
    /**
     * Model popup
     */
    $(".modal-trigger").click(function (e) {
        e.preventDefault();
        dataModal = $(this).attr("data-modal");
        $("#" + dataModal).css({"display": "block"});
        // $("body").css({"overflow-y": "hidden"}); //Prevent double scrollbar.
    });

    $(".close-modal, .modal-sandbox").click(function () {
        $(".modal").css({"display": "none"});
        // $("body").css({"overflow-y": "auto"}); //Prevent double scrollbar.
    });

    $('#badge_colorpicker').on('change', function () {
        $('#badge_hexcolor').val(this.value);
    });
    $('#badge_hexcolor').on('change', function () {
        $('#badge_colorpicker').val(this.value);
    });

    $('#text_colorpicker').on('change', function () {
        $('#text_hexcolor').val(this.value);
    });
    $('#text_hexcolor').on('change', function () {
        $('#text_colorpicker').val(this.value);
    });

    /**
     * shortcode copy function
     * @param element
     */
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }

    /**
     * Shortcode copy event
     */
    $('#awdr_shortcode_copy_btn').on('click', function () {
        copyToClipboard("#awdr_shortcode_text");
        var btn = $(this);
        btn.html(wdr_data.localization_data.copied);
        setTimeout(
            function () {
                btn.html(wdr_data.localization_data.copy_shortcode);
            }, 2000
        );
    });

    /**
     * Woocommerce Tooltip event
     */
    function awdr_trigger_woocommerce_tooltip() {
        $('.tips, .help_tip, .woocommerce-help-tip').tipTip({
            'attribute': 'data-tip',
            'fadeIn': 50,
            'fadeOut': 50,
            'delay': 200
        });
    }

    awdr_trigger_woocommerce_tooltip();

    /**
     * Change Title text dynamically in cuztomizer table
     */
    $('.awdr_popup_col_name_text_box').keyup(function () {
        var column_text = $(this).val();
        var column_class = $(this).data("keyup");
        $('.' + column_class).html(column_text);
    });
    $('.awdr_popup_col_name_text_box').blur(function () {
        var column_text = $(this).val();
        var column_class = $(this).data("keyup");
        $('.' + column_class).html(column_text);
    });

    /**
     * Show/Hide apply discount subsequently row in settings tab
     */
    $('.apply_product_and_cart_discount_to').change(function () {
        let subsequent_class = $(this).data("subsequent");
        if ($(this).val() == 'all') {
            $('.' + subsequent_class).show();
        } else {
            $('.' + subsequent_class).hide();
        }
    });

    /**
     * Bogo Discount Type
     */
    $(document).on('change', '.awdr-bogo-discount-type', function () {
        let get_bogo_free_type = $(this).val();
        let get_bogo_free_type_parent = $(this).attr('data-parent');
        let get_bogo_free_type_siblings = $(this).attr('data-siblings');

        switch (get_bogo_free_type) {
            case 'flat':
                $(this).parent('.' + get_bogo_free_type_parent).siblings('.' + get_bogo_free_type_siblings).show();
                $(this).parent('.' + get_bogo_free_type_parent).siblings('.' + get_bogo_free_type_siblings).find('.wdr_desc_text').text(wdr_data.localization_data.buyx_getx_value);
                break;
            case 'percentage':
                $(this).parent('.' + get_bogo_free_type_parent).siblings('.' + get_bogo_free_type_siblings).show();
                $(this).parent('.' + get_bogo_free_type_parent).siblings('.' + get_bogo_free_type_siblings).find('.wdr_desc_text').text(wdr_data.localization_data.buyx_getx_percentage);
                break;
            case 'free_product':
            default:
                $(this).parent('.' + get_bogo_free_type_parent).siblings('.' + get_bogo_free_type_siblings).hide();
        }
    });

    $(document).on('change', '.awdr-bogo-recurcive', function () {
        let get_bogo_range_parent = $(this).attr('data-recursive-row');
        let get_recursive_parent = $(this).attr('data-recursive-parent');
        let hide_if_recursive = $(this).attr('data-hide-add-range');
        let bogo_max_range = $(this).attr('data-bogo-max-range');
        let bogo_min_range = $(this).attr('data-bogo-min-range');
        let bogo_border_bottom = $(this).attr('data-bogo-border');
        let recursive_length = $('.' + get_bogo_range_parent).length;
        if ($(this).prop("checked") == false) {
            $(this).parents('.awdr_bogo_main').siblings('.' + hide_if_recursive).show();
            $(this).parents('.' + get_recursive_parent).siblings('.' + bogo_max_range).show();
            $(this).parents('.' + get_recursive_parent).siblings().find('.' + bogo_max_range).show();
          //  $('.'+bogo_max_range).show();
            $(this).parents('.' + get_recursive_parent).siblings('.' + bogo_min_range).find('.wdr_desc_text').text(wdr_data.localization_data.recursive_min_qty);
            $('.' + bogo_border_bottom).css("border-bottom", "1px solid #ddd");
            return;
        }
        if (recursive_length > 3) {
            let isRecursive = confirm(wdr_data.localization_data.recursive_warning);
            if (isRecursive) {
                $(this).parents('.' + get_bogo_range_parent).siblings().remove();
            } else {
                $(this).prop("checked", false);
            }
        }
        if ($(this).prop("checked") == true) {
            $(this).parents('.awdr_bogo_main').siblings('.' + hide_if_recursive).hide();
            $(this).parents('.' + get_recursive_parent).siblings('.' + bogo_max_range).hide();
            $(this).parents('.' + get_recursive_parent).siblings().find('.' + bogo_max_range).hide();
            //$('.'+bogo_max_range).hide();
            $(this).parents('.' + get_recursive_parent).siblings('.' + bogo_min_range).find('.wdr_desc_text').text(wdr_data.localization_data.recursive_qty);
            $('.' + bogo_border_bottom).css("border-bottom", "unset");
        }
    });

    $(document).on('change', '.awdr-product-discount-type', function () {
        $('.awdr-example').html('');
        var data_placement = $(this).data('placement');
        if ($(this).val() != "not_selected" && $(this).val() != 'wdr_free_shipping') {
            wdr_buildrule.wdr_clone_field({
                addFilterMethod: '.' + $(this).val(),
                ruleAppendTo: '.' + data_placement,
                addDiscountType: 'enable',
            });
            $(".calculate_cart_from option[value='from_filter']").show();
            $('.adv-msg-discount, .adv-msg-discount-price').show();

            $('.awdr-hidden-new-rule').fadeIn(500);
            $('.awdr-filter-section').fadeIn(500);
            $("button.wdr_save_stay, button.wdr_save_close").attr("disabled", false).removeClass("wdr_save_btn_disabled");
            make_wdr_select2_search($('.' + data_placement).find('[data-field="autocomplete"]'));
            $('.awdr-discount-container').show();
            $('.' + data_placement).find('.bulk_range_setter_group').addClass('bulk_range_setter').attr('id', 'bulk_adjustment_sortable');
            $('.' + data_placement).find('.set_range_setter_group').addClass('set_range_setter').attr('id', 'bulk_adjustment_sortable');
            $('#bulk_adjustment_sortable').sortable();
            $("#bulk_adjustment_sortable").disableSelection();
            $('.awdr-filter-heading').html(wdr_data.localization_data.common_filter_heading);
            $('.awdr-filter-content').html(wdr_data.localization_data.common_filter_description);
            $('.awdr-discount-heading').html(wdr_data.localization_data.common_discount_heading);
            $('.awdr-discount-content').html(wdr_data.localization_data.common_discount_description);
            $('.awdr-rules-content').html(wdr_data.localization_data.common_rules_description);

            if($(this).val() == 'wdr_buy_x_get_y_discount'){
                make_wdr_select2_search($('.' + data_placement ).find('[data-list="product_category"]'));
                $('.adv-msg-min-qty, .adv-msg-max-qty, .adv-msg-discount, .adv-msg-discount-price').hide();
            }
            if($(this).val() == 'wdr_buy_x_get_x_discount'){
                $('.adv-msg-min-qty, .adv-msg-max-qty, .adv-msg-discount, .adv-msg-discount-price').hide();
            }
            if ($(this).val() == 'wdr_bulk_discount') {
                    $('.adv-msg-min-qty, .adv-msg-max-qty, .adv-msg-discount, .adv-msg-discount-price').show();
            } else if ($(this).val() == 'wdr_set_discount') {
                    $('.adv-msg-min-qty, .adv-msg-discount, .adv-msg-discount-price').show();
                    $('.adv-msg-max-qty').hide();
            }
        } else if ($(this).val() == 'wdr_free_shipping') {
            $('.awdr-hidden-new-rule').fadeIn(500);
            $('.awdr-filter-section').fadeOut();
            $("button.wdr_save_stay, button.wdr_save_close").attr("disabled", false).removeClass("wdr_save_btn_disabled");
            $('.' + data_placement).html('');
            $('.awdr-discount-container').hide();
            $('.adv-msg-min-qty, .adv-msg-max-qty, .adv-msg-discount, .adv-msg-discount-price').hide();
            $(".calculate_cart_from option[value='from_filter']").hide();
        } else {
            $("button.wdr_save_stay, button.wdr_save_close").attr("disabled", true).addClass("wdr_save_btn_disabled");
            $('.awdr-hidden-new-rule').fadeOut(500);
        }

        $(this).trigger("advanced_woo_discount_rules_on_change_adjustment_type", [$(this).val()]);
    });
    $(".awdr-product-discount-type").trigger('change');
    if (wdr_data.rule_id == "view") {
        $("button.wdr_save_stay, button.wdr_save_close").attr("disabled", false).removeClass("wdr_save_btn_disabled");
    } else {
        $("button.wdr_save_stay, button.wdr_save_close").attr("disabled", true).addClass("wdr_save_btn_disabled");
    }

    $(document).on('change', '.apply_fee_coupon_checkbox', function () {
        if ($(this).prop("checked") == true) {
            $(this).parents('.page__toggle').siblings('.apply_fee_coupon_label').show();
        } else {
            $(this).parents('.page__toggle').siblings('.apply_fee_coupon_label').hide();
        }
    });

    $(document).on('change', '.bulk_table_customizer_show_hide_column', function () {
        var col_name = $(this).data("colname");
        if ($(this).prop("checked") == true) {
            $('.' + col_name).show();
        } else {
            $('.' + col_name).hide();
        }
    });

    var acc = document.getElementsByClassName("awdr-accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function (e) {
            e.preventDefault();
            this.classList.toggle("awdr-accordion-active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                ///  panel.style.display = "none";
                $(panel).slideUp(1000);
            } else {
                $(panel).slideDown(1000);
                ///panel.style.display = "block";
            }
        });
    }
    $(document).on('change', '.on_sale_badge_condition', function () {
        if($(this).val() === 'disabled'){
            $('.sale_badge_toggle').hide();
            $('.sale_badge_customizer').hide();
        }else{
            $('.sale_badge_toggle').show();
            if($('#customize_on_sale_badge').prop("checked") == true){
                $('.sale_badge_customizer').show();
            }else{
                $('.sale_badge_customizer').hide();
            }
        }
    });
    $(document).on('change', '#customize_on_sale_badge', function () {
        if($(this).prop("checked") == true){
            $('.sale_badge_customizer').show();
        }else{
            $('.sale_badge_customizer').hide();
        }
    });
    $(document).on('change', '#badge_colorpicker', function () {
        let background_color = $(this).val();
        $('.awdr_admin_discount_bar').css('background-color',background_color);
    })
    $(document).on('change', '#badge_hexcolor', function () {
        let background_color = $(this).val();
        $('.awdr_admin_discount_bar').css('background-color',background_color);
    })
    $(document).on('change', '#text_colorpicker', function () {
        let text_color = $(this).val();
        $('.awdr_admin_discount_bar').css('color',text_color);
    })
    $(document).on('change', '#text_hexcolor', function () {
        let text_color = $(this).val();
        $('.awdr_admin_discount_bar').css('color',text_color);
    })
    $(document).on('change', '#awdr_discount_bar_content', function () {
        let discount_bar_content = $(this).val();
        $('.awdr_admin_discount_bar').html(discount_bar_content);
    })
    $(document).on('change', '.awdr_mode_of_operator', function () {
        let mode_of_operator = $(this).val();
        if(mode_of_operator == "variation"){
            $('.awdr-example').html(wdr_data.localization_data.mode_variation_cumulative_example)
        }else{
            $('.awdr-example').html('')
        }
    })
    $(document).on('click', '.awdr-hidden-search', function () {
        let search_string = $('.awdr-hidden-name').val();
        $('.wdr-rule-search-key').val(search_string);
        $('#wdr-search-top').submit();
    });
});
