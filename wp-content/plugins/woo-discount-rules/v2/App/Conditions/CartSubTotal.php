<?php

namespace Wdr\App\Conditions;

use Wdr\App\Controllers\DiscountCalculator;
use Wdr\App\Helpers\Woocommerce;

if (!defined('ABSPATH')) exit;

class CartSubTotal extends Base
{
    function __construct()
    {
        parent::__construct();
        $this->name = 'cart_subtotal';
        $this->label = __('Subtotal', WDR_TEXT_DOMAIN);
        $this->group = __('Cart', WDR_TEXT_DOMAIN);
        $this->template = WDR_PLUGIN_PATH . 'App/Views/Admin/Rules/Conditions/Cart/Subtotal.php';
    }

    public function check($cart, $options)
    {
        if(empty($cart)){
            return false;
        }
        $sub_total_recalculate = false;
        $cart_sub_total = 0;
        if($options->calculate_from == 'from_filter'){
            $cart_sub_total = DiscountCalculator::getFilterBasedCartQuantities('cart_subtotal', $this->rule);
            $cart_sub_total = Woocommerce::round($cart_sub_total);
        }else{
            if(apply_filters('advanced_woo_discount_rules_calculate_cart_subtotal_manually', false)){
                $cart = Woocommerce::getCart(true);
            } else {
                $cart_sub_total = self::$woocommerce_helper->getCartSubtotal();
            }
            $sub_total_recalculate = true;
        }
        if(!empty($cart) && $cart_sub_total == 0 && $sub_total_recalculate){
            foreach ($cart as $cart_product){
                $cart_sub_total += self::$woocommerce_helper->getCartLineItemSubtotal($cart_product);
            }
            $cart_sub_total = Woocommerce::round($cart_sub_total);
        }
        if (isset($options->operator) && $options->value) {
            $operator = sanitize_text_field($options->operator);
            $value = sanitize_key($options->value);
            return $this->doComparisionOperation($operator, $cart_sub_total, $value);
        }

        return false;
    }
}