<?php

if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

    class WC_Gateway_NM_TwoCheckout extends WC_Payment_Gateway{

        // Logging
        public static $log_enabled = false;
        public static $log = false;

        public function __construct(){

            $plugin_dir = plugin_dir_url(__FILE__);

            global $woocommerce;

            $this->id   = 'nmwoo_2co';
            $icon_url   = ($this->get_option( 'image' ) != '' ? $this->get_option( 'image' ) : TWOCO_URL.'/images/2co_logo.png');
			$this->icon = $icon_url;
			$this->method_title 		= '2Checkout - Credit Card/PayPal';
			$this->method_description 	= '2Checkout Payment Gateway for WooCommerce. Setup your 2Checkout account with <a href="http://najeebmedia.com/2checkout-payment-gateway-for-woocommerce/" target="_blank">these setting</a>';
            $this->has_fields = true;

            // Load the settings
            $this->init_form_fields();
            $this->init_settings();

            // Define user set variables
            $this->title = $this->get_option('title');
            $this->seller_id = $this->get_option('seller_id');
            $this->publishable_key = $this->get_option('publishable_key');
            $this->private_key = $this->get_option('private_key');
            $this->description = $this->get_option('description');
            $this->sandbox = $this->get_option('sandbox');
            $this->debug = $this->get_option('debug');

            self::$log_enabled = $this->debug;
            
            $this->demo	= $this->sandbox == 'yes' ? true : false;

            // Actions
            add_action('woocommerce_receipt_' . $this->id, array($this, 'receipt_page'));

            // Save options
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );

            // Payment listener/API hook
            add_action('woocommerce_api_wc_' . $this->id, array($this, 'check_ipn_response'));

        }

        /**
        * Logging method
        * @param  string $message
        */
        public static function log( $message ) {
            if ( self::$log_enabled ) {
                if ( empty( self::$log ) ) {
                    self::$log = new WC_Logger();
                }
                $message = is_array($message) ? json_encode($message) : $message;
                self::$log->add( 'twoco_cc', $message );
            }
        }

        /**
         * Admin Panel Options
         * - Options for bits like 'title' and availability on a country-by-country basis
         *
         * @since 1.0.0
         */
        public function admin_options() {

            ?>
            <h3><?php _e( '2Checkout by N-Media', 'twoco' ); ?></h3>
            <p><?php _e( 'Credit Card Payment Form on WooCommerce Checkout', 'twoco' ); ?></p>

            <table class="form-table">
                <?php
                // Generate the HTML For the settings form.
                $this->generate_settings_html();
                ?>
            </table><!--/.form-table-->
            <?php
        }


        /**
         * Initialise Gateway Settings Form Fields
         *
         * @access public
         * @return void
         */
        function init_form_fields() {

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __( 'Enable/Disable', 'twoco' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable 2Checkout', 'twoco' ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __( 'Title', 'twoco' ),
                    'type' => 'text',
                    'description' => __( 'This controls the title which the user sees during checkout.', 'twoco' ),
                    'default' => __( 'Credit Card/PayPal', 'twoco' ),
                    'desc_tip'      => true,
                ),
                'description' => array(
                    'title' => __( 'Description', 'twoco' ),
                    'type' => 'textarea',
                    'description' => __( 'This controls the description which the user sees during checkout.', 'twoco' ),
                    'default' => __( 'Pay with Credit Card/PayPal', 'twoco' )
                ),
                'seller_id' => array(
                    'title' => __( 'Seller ID', 'twoco' ),
                    'type'          => 'text',
                    'description' => __( 'Please enter your 2Checkout account number; this is needed in order to take payment.', 'twoco' ),
                    'default' => '',
                    'desc_tip'      => true,
                    'placeholder'   => ''
                ),
                'publishable_key' => array(
                    'title' => __( 'Publishable Key', 'twoco' ),
                    'type'          => 'text',
                    'description' => __( 'Please enter your 2Checkout Publishable Key; this is needed in order to take payment.', 'twoco' ),
                    'default' => '',
                    'desc_tip'      => true,
                    'placeholder'   => ''
                ),
                'private_key' => array(
                    'title' => __( 'Private Key', 'twoco' ),
                    'type'          => 'text',
                    'description' => __( 'Please enter your 2Checkout Private Key; this is needed in order to take payment.', 'twoco' ),
                    'default' => '',
                    'desc_tip'      => true,
                    'placeholder'   => ''
                ),
                'sandbox' => array(
                    'title' => __( 'Sandbox/Production', 'twoco' ),
                    'type' => 'checkbox',
                    'label' => __( 'Use 2Checkout Sandbox', 'twoco' ),
                    'default' => 'no'
                ),
                                'debug' => array(
                                    'title'       => __( 'Debug Log', 'twoco' ),
                                    'type'        => 'checkbox',
                                    'label'       => __( 'Enable logging', 'twoco' ),
                                    'default'     => 'no',
                                    'description' => sprintf( __( 'Log 2Checkout events', 'twoco' ), wc_get_log_file_path( 'twocheckout' ) )
                                )
            );

        }

        /**
         * Generate the credit card payment form
         *
         * @access public
         * @param none
         * @return string
         */
        
        public function payment_fields() {
			
			if ( $this->supports( 'tokenization' ) && is_checkout() ) {
				$this->tokenization_script();
				$this->saved_payment_methods();
				$this->form();
				$this->save_payment_method_checkbox();
			} else {
				$this->form();
			}
		}
		
		public function field_name( $name ) {
			return $this->supports( 'tokenization' ) ? '' : ' name="' . esc_attr( $this->id . '-' . $name ) . '" ';
		}
		
		public function form() {
			
			if ( $this->description ) {
				// you can instructions for test mode, I mean test card numbers etc.
				if ( $this->demo ) {
					$this->description .= ' TEST MODE ENABLED. In test mode, you can use the card numbers listed in <a href="#" target="_blank">documentation</a>.';
					$this->description  = trim( $this->description );
				}
				// display the description with <p> tags etc.
				echo wpautop( wp_kses_post( $this->description ) );
			}
			
			
			wp_enqueue_script( 'wc-credit-card-form' );
			
	
			$fields = array();
	
			$cvc_field = '<p class="form-row form-row-last">
				<label for="' . esc_attr( $this->id ) . '-card-cvc">' . esc_html__( 'Card code', 'twoco' ) . '&nbsp;<span class="required">*</span></label>
				<input id="' . esc_attr( $this->id ) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="' . esc_attr__( 'CVC', 'twoco' ) . '" ' . $this->field_name( 'card-cvc' ) . ' style="width:100px" />
			</p>';
	
			$default_fields = array(
				'card-number-field' => '<p class="form-row form-row-wide">
					<label for="' . esc_attr( $this->id ) . '-card-number">' . esc_html__( 'Card number', 'twoco' ) . '&nbsp;<span class="required">*</span></label>
					<input id="' . esc_attr( $this->id ) . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" ' . $this->field_name( 'card-number' ) . ' />
				</p>',
				'card-expiry-field' => '<p class="form-row form-row-first">
					<label for="' . esc_attr( $this->id ) . '-card-expiry">' . esc_html__( 'Expiry (MM/YY)', 'twoco' ) . '&nbsp;<span class="required">*</span></label>
					<input id="' . esc_attr( $this->id ) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="cc-exp" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__( 'MM / YY', 'twoco' ) . '" ' . $this->field_name( 'card-expiry' ) . ' />
				</p>',
			);
	
			if ( ! $this->supports( 'credit_card_form_cvc_on_saved_method' ) ) {
				$default_fields['card-cvc-field'] = $cvc_field;
			}
	
			$fields = wp_parse_args( $fields, apply_filters( 'woocommerce_credit_card_form_fields', $default_fields, $this->id ) );
			?>
	
			<fieldset id="wc-<?php echo esc_attr( $this->id ); ?>-cc-form" class='wc-credit-card-form wc-payment-form'>
				
				<input type="hidden" name="token" id="twoco_token">
				<?php do_action( 'woocommerce_credit_card_form_start', $this->id ); ?>
				<?php
				foreach ( $fields as $field ) {
					echo $field; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
				}
				?>
				<?php do_action( 'woocommerce_credit_card_form_end', $this->id ); ?>
				<div class="clear"></div>
			</fieldset>
			<?php
	
			if ( $this->supports( 'credit_card_form_cvc_on_saved_method' ) ) {
				echo '<fieldset>' . $cvc_field . '</fieldset>'; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
			}
			
		    // let's suppose it is our payment processor JavaScript that allows to obtain a token
			if( $this->demo) {
			    wp_enqueue_script( 'twoco_pk', '//sandbox.2checkout.com/checkout/api/script/publickey/'.$this->seller_id );
			    wp_enqueue_script( 'twoco_js', '//sandbox.2checkout.com/checkout/api/2co.min.js' );
			}else{
			    wp_enqueue_script( 'twoco_pk', '//www.2checkout.com/checkout/api/script/publickey/'.$this->seller_id );
			    wp_enqueue_script( 'twoco_js', '//www.2checkout.com/checkout/api/2co.min.js' );
			}
			
			// and this is our custom JS in your plugin directory that works with token.js
			wp_register_script( 'woocommerce_twoco', TWOCO_URL.'/js/twoco-cc-form.js', array( 'jquery', 'twoco_js' ) );
		 
			// in most payment processors you have to use PUBLIC KEY to obtain a token
			wp_localize_script( 'woocommerce_twoco', 'twoco_vars', array(
				'publishable_key'	=> $this->publishable_key,
				'is_demo'			=> $this->demo,
				'seller_id'			=> $this->seller_id,
				'error_message'		=> __('Please check your Seller ID, Publishable or Private Key', 'twoco'),
			) );
		 
			wp_enqueue_script( 'woocommerce_twoco' );
		}

        /**
         * Process the payment and return the result
         *
         * @access public
         * @param int $order_id
         * @return array
         */
        function process_payment( $order_id ) {
            global $woocommerce;
            $order = new WC_Order($order_id);
            $order_number = $order->get_order_number();

           $this->log( "Payment processing via Credi Card Mode with Order No {$order_number}" );

            $curr_code = get_woocommerce_currency();
    		if( twoco_convertion_is_on() ) {
    			$curr_code = twoco_get_conversion_currency();
    		}
		
            // 2Checkout Args
            $twocheckout_args = array(
                                    'token'         => $_POST['token'],
                                    'sellerId'      => $this->seller_id,
                                    'currency'      => $curr_code,
                                    'total'         => twoco_get_price($order->get_total()),
                                    // 'lineItems'     => array($this->get_subscripttions_details()),

                                    // Order key
                                    'merchantOrderId'    => $order_number,

                                    // Billing Address info
                                    "billingAddr" => array(
                                        'name'          => $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
                                        'addrLine1'     => $order->get_billing_address_1(),
                                        'addrLine2'     => $order->get_billing_address_2(),
                                        'city'          => $order->get_billing_city(),
                                        'state'         => $order->get_billing_state(),
                                        'zipCode'       => $order->get_billing_postcode(),
                                        'country'       => $order->get_billing_country(),
                                        'email'         => $order->get_billing_email(),
                                        'phoneNumber'   => $order->get_billing_phone()
                                    )
                                );
            // twoco_pa($twocheckout_args);
            Twocheckout::privateKey($this->private_key); 
			Twocheckout::sellerId($this->seller_id); 
            if ($this->demo) {
				Twocheckout::sandbox(true);
            }
			try {
                  
                $charge = Twocheckout_Charge::auth($twocheckout_args);
                $this->log($charge);
                if (isset($charge['response']['responseCode']) && $charge['response']['responseCode'] == 'APPROVED') {
                	$order_number = $charge['response']['orderNumber'];
                	
                	do_action('twoco_after_succesful_cc_payment', $charge, $this);
                	
                	$this->log(" == ORDER PROCESSED/COMPLETED == ORDER_NO = {$order_number}");
                	
                	$order->add_order_note( sprintf(__('Payment completed via 2Checkout Order Number %d', '2checkout'), $order_number) );
                    $order->payment_complete();
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url( $order )
                    );
                }
            } catch (Twocheckout_Error $e) {
                // twoco_pa($e);
                wc_add_notice($e->getMessage(), $notice_type = 'error' );
                return;
            }     
        }
        
        // Testing Subscriptions
        function get_subscripttions_details() {
            
            $subscription = array('type'    => 'product',
                                    'name'  => 'New Subscriptions',
                                    'recurrence' => '1 Week',
                                    'duration'  => '1 Month',
                                    'price' => 100,
                                    'startupFee'    => 20);
                                    
            return $subscription;
        }

    }

    // include TWOCO_PATH.'/lib/twoco-php/lib/Twocheckout.php';