/*  (function (document, src, libName, config) {
    var script             = document.createElement('script');
    script.src             = src;
    script.async           = true;
    var firstScriptElement = document.getElementsByTagName('script')[0];
    script.onload          = function () {
        for (var namespace in config) {
            if (config.hasOwnProperty(namespace)) {
                window[libName].setup.setConfig(namespace, config[namespace]);
            }
        }
        window[libName].register();
        
        TwoCoInlineCart.events.subscribe('cart:closed', function (e) {

 			console.log('cart closed');
	        console.log(e);
	    });
	    
	    TwoCoInlineCart.events.subscribe('payment:finalized', function (e) {

 			console.log('payment:finalized');
	        console.log(e);
	    });
	    
	    TwoCoInlineCart.events.subscribe('fulfillment:finalized', function (e) {

 			console.log('fulfillment:finalized');
	        console.log(e);
	    });
	    
	   twoco_init_inline( twoco_vars.order_data );
    };

    firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
})(document, 'https://secure.avangate.com/checkout/client/twoCoInlineCart.js', 'TwoCoInlineCart',{"app":{"merchant":twoco_vars.seller_id},"cart":{"host":"https:\/\/secure.2checkout.com","customization":"inline"}});*/


jQuery(function($){
    
    // alert('hi');
    
    setTimeout(function(){ 
        
        if( twoco_vars.inline_display === 'auto_popup' ) {
            twoco_init_inline( twoco_vars.order_data );
        }
        
    }, 500);
    
    $("#twco-btn-form").on('click', function(){
        twoco_init_inline( twoco_vars.order_data );
    });
});

function twoco_init_inline( order_data ) {
 
    // console.log(order_data);
    
   	TwoCoInlineCart.setup.setMerchant( twoco_vars.seller_id); // your Merchant code
	TwoCoInlineCart.setup.setMode('DYNAMIC');

    TwoCoInlineCart.billing.setName( order_data.full_name ); // customer billing Name
    TwoCoInlineCart.billing.setEmail( order_data.email ); // customer email address
    TwoCoInlineCart.billing.setCountry( order_data.country ); // customer billing country
    
    TwoCoInlineCart.products.add({
        // code: 'D6B9146F46',
        type: 'PRODUCT',
        name: twoco_vars.checkout_title,
        price: order_data.order_amount,
        quantity: 1,
        tangible: false,
    });
    
    if( twoco_vars.is_demo === '1' ) {
        
        TwoCoInlineCart.cart.setTest(true);
        TwoCoInlineCart.billing.setName( 'John Doe' );
    }
    // TwoCoInlineCart.cart.addCoupon('50%OFF'); // discount code
    TwoCoInlineCart.cart.setCurrency( twoco_vars.wc_currency ); // order currency
    TwoCoInlineCart.cart.setLanguage( twoco_vars.wp_lang ); // language code
    TwoCoInlineCart.cart.setOrderExternalRef( order_data.order_id ); // order external reference
    // TwoCoInlineCart.cart.setExternalCustomerReference('ID123extern'); // external customer reference
    // TwoCoInlineCart.cart.setReturnMethod( order_data.redirect );
    TwoCoInlineCart.cart.setReturnMethod({
        type: 'redirect',
        url : order_data.redirect
    });
    TwoCoInlineCart.cart.checkout(); // start checkout process
// 	return false;
 
}