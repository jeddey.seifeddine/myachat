=== Direct checkout, Add to cart redirect, Quick purchase button, Buy now button, Quick View button for WooCommerce ===
Contributors: rajeshsingh520
Donate link: piwebsolution.com
Tags: WooCommerce direct checkout, direct checkout, WooCommerce Single Page Checkout, one page checkout, redirect to checkout
Requires at least: 3.0.1
Tested up to: 5.3.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add to cart will redirects you to checkout page or your selected custom page, Quick purchase button WooCommerce, Buy now button WooCommerce, Quick View product

== Description ==

* **Redirect to checkout** page after add to cart
* Redirect even works with **Ajax add to cart** option
* You can change **redirect page**, and make it other then checkout page
* You can redirect to even custom url of your site, so you can redirect them to category page after add to cart so they can see some more similar product
* **Remove continue shopping link** that is shown when you add product to cart
* **Disable cart page**
* **Show cart on checkout page**, so user can modify its cart on checkout page itself
* **No need for ShortCode** to make this work
* Change add to cart button text
* Quick purchase button can be enabled on product page or product archive pages like shop or category
* Quick buy support variable product, grouped product as well
* Quick view option for the product so customer can see the product from the archive page, in the form of a popup

<blockquote>
Buy Now button for Variable product on Category / Shop / Archive page, so buyer can directly add the variable product to cart even without seeing its variation. **What it does is it adds the First variation of the variable product in the cart.**
</blockquote>

**PRO Features**

* **Product specific overwrite** of global redirect setting
* **Change the redirect page** for a specific product
* Change redirect page for a specific product even on **archive product**
* No other plugin provide product-specific redirect page in **Ajax add to cart**
* **Disable redirect** for a specific product
* Set **redirect on a specific product** only
* Set custom redirect url on per product basis, so when they add that particular product in cart they get redirected to that specific link that you have set for that product, this will increase your sales. 
* **Premium support:** We respond to all support topics within 24 hours
* You can modify the label of Buy now button for product page and archive page
* Change position of buy now button
* Change Quick purchase button redirect to cart or checkout page 
* Disable quick purchase button for particular product from product overwrite
* Change background color of the Quick view module
* Change text color of the quick view module
* You can redirect to external url, which is from outside your website.


== Frequently Asked Questions ==

= Where are the setting of the plugin? =
On the left side of the dashboard
WooCommerce > Direct checkout

= How to activate redirect =
Just install the plugin and activate it, and it will start redirecting to checkout page

= I want to change the redirect page =
Go to plugin Basic setting see the option **Redirect to page** there you can select from pages

= I want to disable redirect =
Go to Basic Setting click on **Enable redirect on add to cart** this is used to enable and disable the redirect

= I want to set redirect on on product only =
PRO version has this Features
In pro version you can disable global redirect and set redirect for you one product.
This way user will be redirected only when they Add to cart that specific product, and they wont be redirected when they add to cart other products

= I want to disable redirect on few specific products =
PRO version allows you to do this

= I want to change the redirect page for specific product =
Pro Version allows you to change the redirect page for specific product

= Will it work on Archive page =
Yes our plugin works on all the add to cart button

= Will it work on Ajax add to cart button =
Yes our plugin works on ajax add to cart button as well

= I want to bypass cart page in WooCommerce =
You can do so now with our option "Disable cart page" this will redirect all the cart page traffic to checkout page

= I want to show cart page and checkout page as one page checkout instead of 2 different pages in WooCommerce
You can do that using our option "Enable single page checkout" after enabling this cart and checkout will shown on same page

= I want to redirect to category page after add to cart =
You can do that in FREE version your can have one category page as default redirect,
But in PRO version you can set product lever redirect, so if some one buys a Pen you can redirect them to Books category or even a single Book (product)
This way you can create a complete funnel of redirects

= Increase sell by redirecting customer to different product then they buy one =
Say some one buys T-shirt then as soon as they add to cart this t-shirt, they get redirected to Paints product page, say if they even add some pain in cart they will be redirected to some other product they you have set on the particular product, this way they will go on buying 

= I want to change add to cart button text =
Yes you can change it from the plugin setting

= You can add Quick purchase / Buy now button =
When user click this button they will be directly taken to the checkout page

= You can change label of buy now button =
In pro version you can change label of buy now button, and have different labels for product page and product archive pages

= You can disable buy now button for the product archive page =
Yes you can disable it for product archive page and keep it running for the product page or vice versa

= I want to disable buy now / quick purchase button for specific product =
Yes you can do that in pro version, it allows you to disable buy now button on particular product

= Buy now button is not working on variable product =
At present it is not working in variable product but it will start supporting variable product shortly

= You can have a Quick view option for the product in archive page =
Using the Quick view button customer can see the product detail from the archive page without leaving the archive page

= Quick view will work for the variable product =
Ues Quick view will work for the variable product, infect it is most useful for the variable product, as customer can see the variable product directly form the archive page and even add them to cart from the archive page, as the Quick view module allow them to select the product variation

= Remove Order comment and coupon field from checkout page =
You can remove order comment and coupon filed from the checkout page 

= Remove "Ship to a different address?" =
In Pro version you can remove "Ship to a different address?" option 

= Remove billing field from the checkout form =
In Pro version You can remove, Billing first name, Billing last name, Billing city, Billing country, Billing state , Billing address line 1 , Billing address line 2, Billing postal code from the checkout form

= Remove shipping field from the checkout form =
In Pro version You can remove, Shipping first name, Shipping last name, Shipping city, Shipping country, Shipping state , Shipping address line 1 , Shipping address line 2, Shipping postal code from the checkout form

= I want to redirect to external url after add to cart =
Yes you can do that in pro version, in pro version you have the option to enter the list of domain name where you want to redirect, if the external url domain is in this list then redirect will be done, if it is not in this list then redirect to that domain will not work

= I want to show the buy now button on archive page for variable product =
Pro version does that, when they buyer will click this buy now, it will add the First variation of the product in his cart. 

= The buy now button on the archive page is not working for a particular product =
You have to make sure that you have set the default values of all the required variable for that product. If some required variable is not set for the first variation and you have not set a default for that variable then in that case buy now for that product will fail with a warning that "required field can be blank"

= I want the product name given on the Checkout page to be linked to product pages of the respective products =
Yes you can do that using the Pro version it gives you option to link product name on the checkout page to there respective product pages