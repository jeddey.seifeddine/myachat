

<div class="wpwoof-box">
<h4 class="wpwoofeed-section-heading">Feed Options (PRO)</h4>
<div style="float:right;"><span class="unlock_pro_features">Unlock all PRO features: <a target="_blank" href="http://www.pixelyoursite.com/product-catalog-facebook">Click here for a discount</a></span></div>
<div class="wpwoof-addfeed-top">
    <div class="addfeed-top-field wpwoof-open-popup-wrap">
        <p>
            <label class="addfeed-top-label not_in_free">Filter by Category</label>
                <span class="addfeed-top-value not_in_free">
                    <a href="javascript:void(0);" class="wpwoof-button wpwoof-button-blue wpwoof-open-popup">Chose WooCommerce Categories for this Feed</a>
                </span>
        </p>
    </div>
    <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label not_in_free">Filter by Tags</label>
                <span class="addfeed-top-value not_in_free">
                    <textarea></textarea>
                </span>
        </p>
        <p class="description"><span></span><span>Add multiple tags separated by comma.</span></p>
    </div>

    <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label not_in_free">Filter by Sale</label>
                <span class="addfeed-top-value not_in_free">
                    <select>
                        <option  value="all">All Products</option>
                        <option  value="sale">Only products on sale</option>
                        <option  value="notsale">Only products not on sale</option>
                    </select>
                </span>
        </p>
        <p class="description"><span></span><span>Select all products, only sale products or only non sale products.</span></p>
    </div>

    <div class="addfeed-top-field wpwoof-open-popup-wrap">
        <p>
            <label class="addfeed-top-label not_in_free">Filter by Product type</label>
                <span class="addfeed-top-value not_in_free">
                    <a href="javascript:void(0);" class="wpwoof-button wpwoof-button-blue" >Chose WooCommerce Product type for this Feed</a>
                </span>
        </p>
    </div>

    <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label not_in_free">Filter by Stock</label>
                <span class="addfeed-top-value not_in_free">
                    <select name="feed_filter_stock">
                        <option  value="all">All Products</option>
                        <option  value="instock">Only in stock</option>
                        <option  value="outofstock">Only out of stock</option>
                    </select>
                </span>
        </p>
    </div>

    <hr class="wpwoof-break wpwoof-break-small"/>

    <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label not_in_free">Variable Product Price</label>
                <span class="addfeed-top-value not_in_free">
                    <select>
                        <option  value="small">Smaller Price</option>
                        <option  value="big">Bigger Price</option>
                        <option  value="first">First Variation Price</option>
                    </select>
                </span>
        </p>
        <p class="description"><span></span><span>Select which price to be use for main product when there are variations.</span></p>
    </div>
    
    <hr class="wpwoof-break wpwoof-break-small"/>

    <div class="addfeed-top-field" >
        <p>
            <label class="addfeed-top-label not_in_free" >Remove product variations</label>
                <span class="addfeed-top-value not_in_free">
                    <input type="checkbox" value="1" >
                </span>
        </p>
        <p class="description"><span></span><span>Check to remove variations from this feed.</span></p>


        <p>
            <label class="addfeed-top-label not_in_free" >Show main variable product item</label>
                <span class="addfeed-top-value not_in_free">
                    <input disabled type="checkbox"  checked >
                </span>
        </p>
        <p class="description"><span></span><span>Check to show main variable item.</span></p>

        <p>
            <label class="addfeed-top-label not_in_free" >Show main grouped product item</label>
                <span class="addfeed-top-value not_in_free">
                    <input disabled  type="checkbox"  checked >
                </span>
        </p>
        <p class="description"><span></span><span>Check to show main grouped item.</span></p>

        <p>
            <label class="addfeed-top-label not_in_free" >Show main bundle product item</label>
                <span class="addfeed-top-value not_in_free">
                    <input disabled  type="checkbox"  checked >
                </span>
        </p>
        <p class="description"><span></span><span>Check to show main bundle item.</span></p>



    </div>
    <div style="float:right;"><span class="unlock_pro_features">Unlock all PRO features: <a target="_blank" href="http://www.pixelyoursite.com/product-catalog-facebook">Click here for a discount</a></span></div>
</div>
</div>