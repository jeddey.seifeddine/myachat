</div><!-- #content -->
<?php if( !is_page_template('page-blank.php') ): ?>
<footer id="colophon" class="site-footer">
	<?php
	if ( is_active_sidebar( 'footer-top' ) || is_active_sidebar( 'footer-middle' ) || is_active_sidebar( 'footer-bottom' ) ) :		
		if ( is_active_sidebar( 'footer-top' ) ) { ?>
		<div class="widget-column footer-top">
			<div class="container">
				<?php dynamic_sidebar( 'footer-top' ); ?>
			</div>
		</div>
		<?php }
		if ( is_active_sidebar( 'footer-middle' ) ) { ?>
		<div class="widget-column footer-middle">
			<div class="container">
				<?php dynamic_sidebar( 'footer-middle' ); ?>
			</div>
		</div>
		<?php }
		if ( is_active_sidebar( 'footer-bottom' ) ) { ?>
		<div class="widget-column footer-bottom">
			<div class="container">
				<?php dynamic_sidebar( 'footer-bottom' ); ?>
			</div>
		</div>
		<?php } ?>

	<?php endif; ?>
</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- .site-content-contain -->
</div><!-- #page -->
<div class="ftc-close-popup"></div>

<?php 
global $smof_data;
if( ( !wp_is_mobile() && $smof_data['ftc_back_to_top_button'] ) || ( wp_is_mobile() && $smof_data['ftc_back_to_top_button_on_mobile'] ) ): 
	?>
	<div id="to-top" class="scroll-button">
		<a class="scroll-button" href="javascript:void(0)" title="<?php esc_html_e('Back to Top', 'ornaldo'); ?>"><?php esc_html_e('Back to Top', 'ornaldo'); ?></a>
	</div>
<?php endif; ?>
<?php if((isset($smof_data['ftc_enable_popup']) && $smof_data['ftc_enable_popup']) && is_active_sidebar('popup-newletter')) : ?>
	<?php ftc_popup_newsletter(); ?>
<?php endif;  ?>
<?php wp_footer(); ?>

</body>
</html>
