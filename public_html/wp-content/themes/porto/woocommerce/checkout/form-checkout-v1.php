<?php
/**
 * Checkout Form V1
 *
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$porto_woo_version = porto_get_woo_version_number();
$checkout          = WC()->checkout();

// filter hook for include new pages inside the payment method
$get_checkout_url = version_compare( $porto_woo_version, '2.5', '<' ) ? apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ) : wc_get_checkout_url(); ?>
<style type="text/css">
		 span.selection ,input[type="text"] ,select , input[type="tel"] , input[type="email"]{
		 	border: 1px transparent solid;
			background-clip: padding-box;
			border-radius: 5px;
			display: block;
			-webkit-box-sizing: border-box;
			box-sizing: border-box;
			width: 100%;
			padding: 0.92857em 0.78571em;
			word-break: normal;
			line-height: inherit;
			background-color: white;
			color: #333333;
			border-color: #d9d9d9;
		};


</style>
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

<div class="row" id="">
	<div class="col-lg-6" id="">
			<div class="checkout-order-review featured-box featured-box-primary align-left">
		<div class="box-content">
			<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'porto' ); ?></h3>

			<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

			<div id="order_review" class="woocommerce-checkout-review-order">
				<?php do_action( 'woocommerce_checkout_order_review' ); ?>
			</div>

			<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
		</div>
	</div>
	</div>
	<div class="col-lg-6" id="">
			<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="row" id="customer_details">
			<div class="col-lg-12">
				<div class="featured-box featured-box-primary align-left">
					<div class="box-content">
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
					</div>
				</div>
			</div>

	<!-- 		<div class="col-lg-6">
				<div class="featured-box featured-box-primary align-left">
					<div class="box-content">
						<?php do_action( 'woocommerce_checkout_shipping' ); ?>
					</div>
				</div>
			</div> -->
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>
	</div>
</div>





	<div class="row">
			<div class="col-lg-6">
				<div class="featured-box featured-box-primary align-left">
					<div style="border-top-color: #D6E9DE;border-bottom: : 4px solid #9CC146;border-bottom-color: #9CC146" class="box-content" id="moved-payment-layout">
						<div class="row">
							<div style="width: 50%" >
								
								<img style="width: 200px" src="https://handykam.com/wp-content/uploads/2018/11/secure-payment-page-960x300.gif" />
							</div>
							<div style="width: 50%">
								
								<img style="width: 200px" src="https://www.fdrr.ro/wp-content/uploads/2017/08/SSL-Secure-Connection.jpg" />
							</div>

								
								
								
						<div>
					</div>
				</div>
		</div>
	</div>


</form>
