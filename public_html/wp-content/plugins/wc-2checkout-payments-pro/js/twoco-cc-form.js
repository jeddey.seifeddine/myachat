"use strict"
var successCallback = function(data) {

	var checkout_form = jQuery('form.woocommerce-checkout');

	// add a token to our hidden input field
	console.log(data);
	var token = data.response.token.token;
	checkout_form.find('#twoco_token').val(token);

	// deactivate the tokenRequest function event
	checkout_form.off('checkout_place_order', tokenRequest);

	// submit the form now
	checkout_form.submit();

};

var errorCallback = function(error) {

	if (error.errorCode == 300) {

		alert(error.errorMsg);
	}
};

var tokenRequest = function() {

	// here will be a payment gateway function that process all the card data from your form,
	// maybe it will need your Publishable API key which is twoco_params.publishableKey
	// and fires successCallback() on success and errorCallback on failure
	// Setup token request arguments

	var expiry_date = jQuery('#nmwoo_2co-card-expiry').val();
	expiry_date = expiry_date.split('/')
	var exp_month = expiry_date[0].trim();
	var exp_year = expiry_date[1].trim();

	var args = {
		sellerId: twoco_vars.seller_id,
		publishableKey: twoco_vars.publishable_key,
		ccNo: jQuery("#nmwoo_2co-card-number").val(),
		cvv: jQuery("#nmwoo_2co-card-cvc").val(),
		expMonth: exp_month,
		expYear: exp_year
	};

	TCO.requestToken(successCallback, errorCallback, args);

	return false;

};

jQuery(function($) {

	// Pull in the public encryption key for our environment
	const environment = twoco_vars.is_demo === "1" ? 'sandbox' : 'production';
	TCO.loadPubKey(environment);

	var selected_gateway = jQuery("input[name='payment_method']:checked").val();

	if (typeof selected_gateway !== "undefined") {
		//only run 2checkout functions if it's selected 
		if (selected_gateway === "nmwoo_2co") {

			var checkout_form = $('form.woocommerce-checkout');
			checkout_form.on('checkout_place_order', tokenRequest);
		}
	}
});
