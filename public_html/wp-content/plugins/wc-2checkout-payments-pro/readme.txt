=== 2Checkout Payment Gateway for WooCommerce ===
Contributors: nmedia
Tags: payment gateway, 2co payment gateway, 2checkout woocommerce payment, woocommerce payment gateway
Donate link: http://www.najeebmedia.com/donate
Requires at least: 3.5
Tested up to: 5.3
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

It's a WooCommerce extension allow clients make payment with 2Checkout.

== Description ==
N-Media integrated 2Checkout Payment Gateway with WooCommerce. It is tightly integrated with WooCommerce Core API. When order is placed, user is redirected to 2Checkout hosted payment page. All user billing and shipping detail is posted on payment page so customer will not be bother by typing billing and shipping form again on 2Checkout hosted page.

**Features**
<ol>
<li>Itemized Checkout - will display each item with SKU/ID</li>
<li>Pass all billing and shipping data to 2CO purchase page</li>
<li>Enable/Disable Test Mode</li>
</ol>

**Inline 2Checkout - Live PRO**
Checkout experience without leaving your Store.
[More detail About Pro Versoin](http://www.najeebmedia.com/2checkout-payment-gateway-for-woocommerce/ "Inline Checkout - Pro Feature")

== Installation ==
1. Upload plugin directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the `Plugins` menu in WordPress
3. After activation, you can set options from `WooCommerce -> Settings -> Checkout` menu

== Frequently Asked Questions ==
= How to setup my 2Checkout Account? =
You must have a 2Checkout account for get your Account Number and set the return URL and notification URL. So go to 2Checkout and log in.

= How to found my Seller/Account Number? =
In 2Checkout Seller Area you will find your Account Number on the top right of the screen by clicking in the profile icon.

= How set page redirect =
Go into your **Account -> Site Management** and setup:

* Set the **Direct Return** as **Header Redirect (Your URL)**.
* Fill the Approved URL using a URL like:[http://yourdomain.com/?wc-api=wc_gateway_nm_twocheckout]
* Set your **Secret Word**
* Now you need to set **Notification URL** [See how](http://www.najeebmedia.com/best-payment-gateway-to-accept-payment-for-your-business "2Checkout Guide")
* Enable the **Order Created, Fraud Status Changed, Invoice Status Changed and Refund Issued** options and set the URLs like: [http://yourdomain.com/?wc-api=wc_gateway_nm_twocheckout]. [See how](http://www.najeebmedia.com/best-payment-gateway-to-accept-payment-for-your-business "2Checkout Guide")

= I am new to 2Checkout, can I have some quick overview? =
Yes, please visit this [Quick 2Checkout Guide](http://www.najeebmedia.com/best-payment-gateway-to-accept-payment-for-your-business "2Checkout Guide")


== Screenshots ==
1. 2Checkout options
2. WooCommerce Checkout page
3. Itemized checkout

== Changelog ==
= 8.0 February 4, 2020 =
* Feature: Credit Card Form payment options added
* Feature: Inline ConvertPlus payment options added
* Feature: Inline Legacy payment options added
* Feature: Standarch Checkout payment options added
* Bug fixed: Payment Tempering issue fixed
= 7.3 September 28, 2019 =
* PayPal Direct Payment is now separate Checkout
= 7.2 September 23, 2019 =
* Featuer: Direct PayPal is not setup as separate payment option, so it can be used with 2Checkout Credit/Debit Cart Payment.
= 7.1 Sep 7, 2019 =
* Bug fixed: Inline Checkout form fixed.
= 7.0 May 13, 2019 =
* Bug fixed: [Order Status auto update when payment passed](https://wordpress.org/support/topic/order-pending-still-an-issue/)
* Bug fixed: [Download Link not sent in email, now fixed](https://wordpress.org/support/topic/digital-purchases-no-download-link/)
= 6.1 May 7, 2019 =
* Bug Fixed: [Regular checkout return page was giving error out "-1"](https://clients.najeebmedia.com/forums/topic/empty-cart-error-with-plugin/)
= 6.0 April 1, 2019 =
* Feature: Inline Checkout Featuer Added.
= 5.3 March 14, 2019 =
* Bug fixed: Corrency converter now required API Key with API, it's added'
= 5.1 February 12, 2019 =
* Bug fixed: [Error on checkout due to API is fixed](https://clients.najeebmedia.com/forums/topic/please-check-your-seller-id-publishable-or-private-key/)
= 5.0 November 28, 2018 ==
* Bug fixed: [JS error conflict removed when it's not selected](https://clients.najeebmedia.com/forums/topic/plugin-js-runs-when-other-payments-gateways-selected-causing-js-error/)
= 4.2 August 25, 2018 ==
* Feature: An error message will be shown for unauthorized action while making payment via Credit Card
= 4.1 August 15, 2018 ==
* Feature: Old and New WooCommerce Compatible
= 4.0 August 7, 2018 ==
* New Feature: Credit Card Payment on Site
* New Feature: Skip Checkout Step
* New Feature: New Currecny Converter API with free.currencyconverterapi.com
* New Feature: PayPal Direct Feature
* New Feature: Compatible with 2Checkout New API 2.0
* New Feature: Add 2Checkout Order Number in Order Notes
= 3.1 April 25, 2017 ==
* WooCommerce 3.0 Compatible
* Bug Fixed: Total amount calculation error fixed
= 3.0 February 23, 2017 ==
* Bug Fixed: Checkout breaks when currency converter feature is enabled. Now it's handled in script.
= 2.7 February 23, 2016 ==
* Bug Fixed: Inline Checkout was sending 'key' query string in URl to conflict with 2co native args, it's removed.
= 2.6 =
* Bug fixed: shipping address now being copied to 2co
* Feature: Non supported currency now can be convertd to USD using Yahoo Live exchange rate.
= 2.5 = 
* Custom image for checkout page
* Bug Fixed: extra fees now being added to checkout page
= 2.4 =
* Currecny Code added to checkout
= 2.3 =
* BUG Fixed: Variable products prices were not correct, now it's fixed
* Set Product as Tangible or Intangible
* Sending Product ID to Cart Data
= 2.2 =
* fixed return url issue
= 2.1 =
* get_shipping() function is replaced with get_total_shipping
= 2.0 =
* Hash Mismatch issue fixed when client redirected to shop from 2checkout payment

== Upgrade Notice ==
Nothing