<?php 
/*
 Plugin Name: WooCommerce 2Checkout Payment Gateway Version PRO
Plugin URI: https://najeebmedia.com/wordpress-plugin/woocommerce-2checkout-payment-gateway-with-inline-support/
Description: WooCommerce 2Checkout Gateway with Inline Support
Version: 8.0
Author: N-Media
WC requires at least: 3.0.0
WC tested up to: 3.7.0
Author URI: http://www.najeebmedia.com/
*/

define('TWOCO_PATH', untrailingslashit(plugin_dir_path( __FILE__ )) );
define('TWOCO_URL', untrailingslashit(plugin_dir_url( __FILE__ )) );


include plugin_dir_path(__FILE__).'inc/functions.php';


add_action( 'plugins_loaded', 'init_nm_woo_gateway', 0);
function nm_2co_settings( $links ) {
    $payment_url    = admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_gateway_nm_twocheckout' );
    $payment_title  = 'Payment Settings';
    $settings_link  = sprintf(__('<a href="%s">%s</a>','twoco'), $payment_url, $payment_title);
  	array_push( $links, $settings_link );
  	return $links;
}
$plugin = plugin_basename( __FILE__ );
add_filter( "plugin_action_links_$plugin", 'nm_2co_settings' );

// Adding setting tab in WooCommerce
add_filter( 'woocommerce_settings_tabs_array', 'twoco_add_settings_tab', 50 );
function twoco_add_settings_tab($settings_tabs){
	$settings_tabs['twoco_settings'] = __( '2Checkout', 'ppom' );
    return $settings_tabs;
}

// Display settings
add_action( 'woocommerce_settings_tabs_twoco_settings', 'twoco_settings_tab' );
// Save settings
add_action( 'woocommerce_update_options_twoco_settings', 'twoco_save_settings' );

function twoco_settings_tab() {
	
	woocommerce_admin_fields( twoco_get_settings() );
}
function twoco_save_settings() {
    	
	woocommerce_update_options( twoco_get_settings() );
}

function init_nm_woo_gateway(){

    // Twoco payment gateway
    
    $twoco_payment_mode = get_option( 'towco_payment_mode' );	// Get from options
    
    
    if( $twoco_payment_mode == 'convert_plus' ) {
    	
		include plugin_dir_path(__FILE__).'inc/twoco.convert-plus.php';	
    } elseif( $twoco_payment_mode == 'legacy_inline' ) {
    	include plugin_dir_path(__FILE__).'inc/twoco.legacy-inline.php';
    }elseif( $twoco_payment_mode == 'twoco_cc_form' ) {
    	include plugin_dir_path(__FILE__).'lib/twoco-php/lib/Twocheckout.php';
    	include plugin_dir_path(__FILE__).'inc/twoco.cc-form.php';
    }else{
    	include plugin_dir_path(__FILE__).'inc/twoco.standard-checkout.php';
    }
    
    $two_direct_paypal_enable = get_option('twoco_paypal_direct');
    if( $two_direct_paypal_enable == 'yes' ) {
		include plugin_dir_path(__FILE__).'inc/twoco.paypal.php';
    }
	
	// include plugin_dir_path(__FILE__).'inc/direct-pay.php';	
}


function add_nm_payment_gateway( $methods ) {
	$methods[] = 'WC_Gateway_NM_TwoCheckout';
	$methods[] = 'WC_Gateway_NM_PayPal';
	return $methods;
}

add_filter( 'woocommerce_payment_gateways', 'add_nm_payment_gateway' );