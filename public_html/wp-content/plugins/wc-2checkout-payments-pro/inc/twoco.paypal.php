<?php
/**
 * 2Checkout payment gateway class
 **/
 
 if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

class WC_Gateway_NM_PayPal extends WC_Payment_Gateway {

	// Logging
    public static $log_enabled = false;
    public static $log = false;
    
	var $seller_id;
	var $plugin_url;

	public function __construct(){
		
		global $woocommerce;

		$this -> plugin_url = TWOCO_URL;
		$this->has_fields   		= false;
		$this->id 					= 'nmwoo_2co_paypal';
		$this->method_title 		= __('Direct PayPal by 2Checkout','twoco');
		$this->method_description 	= __('This is 2Checkout payment option to redirect user to PayPal directly and bypass 2Checkout payment screen','twoco');
		$this->checkout_url     	= 'https://www.2checkout.com/checkout/purchase';
		$this->checkout_url_sandbox	= 'https://sandbox.2checkout.com/checkout/purchase';
		$icon_url					= ($this->get_option( 'image' ) != '' ? $this->get_option( 'image' ) : $this -> plugin_url.'/images/paypal-2co.png');
		$this->icon 				= $icon_url;
		$this->title 				= $this->get_option( 'title' );
		$this->description 			= $this->get_option( 'description' );
		$this->seller_id			= $this->get_option( 'seller_id' );
		$this->secret_word			= trim($this->get_option( 'secret_word' ));
		$this->demo 				= $this->get_option('demo');
		$this->debug 				= $this->get_option('debug');
		
		self::$log_enabled			= $this->debug;
		$this->demo					= $this->demo == 'yes' ? true : false;
		
		$this->init_form_fields();
		$this->init_settings();
		
			
		// Save options
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		//add_action('process_2co_ipn_request', array( $this, 'successful_request' ), 1 );
		
		// Payment listener/API hook
		add_action( 'woocommerce_api_wc_gateway_nm_twocheckout', array( $this, 'twocheckout_response' ) );
		
		
	}
	
	
	/**
    * Logging method
    * @param  string $message
    */
    public static function log( $message ) {
        if ( self::$log_enabled ) {
            if ( empty( self::$log ) ) {
                self::$log = new WC_Logger();
            }
            
            $message = is_array($message) ? json_encode($message) : $message;
            self::$log->add( 'twocheckout', $message );
        }
    }
	
	function init_form_fields(){
	
		$this->form_fields = array(
				'enabled' => array(
						'title' => __( 'Enable Direct PayPal', 'twoco' ),
						'type' => 'checkbox',
						'label' => __( 'Yes', 'twoco' ),
						'default' => 'yes'
				),
				'seller_id' => array(
						'title' => __( '2CO Account #', 'twoco' ),
						'type' => 'text',
						'description' => __( 'This Seller ID issued by 2Checkout', 'twoco' ),
						'default' => '',
						'desc_tip'      => true,
				),
				'title' => array(
						'title' => __( 'Title', 'twoco' ),
						'type' => 'text',
						'description' => __( 'This controls the title which the user sees during checkout.', 'twoco' ),
						'default' => __( '2Checkout Payment', 'twoco' ),
						'desc_tip'      => true,
				),
				'description' => array(
						'title' => __( 'Customer Message', 'twoco' ),
						'type' => 'textarea',
						'default' => 'Please enter valid credit cart detail.',
				),
				'image' => array(
						'title' => __( 'Image URL', 'twoco' ),
						'type' => 'text',
						'description' => __( 'Use your own image at checkout page by pasting image URL from Media Library. Left blank for default.', 'twoco' ),
						'default' => __( '', 'twoco' ),
						'desc_tip'      => true,
				),
				'secret_word' => array(
						'title' => __( 'Secret Word', 'woocommerce' ),
	                    'type' 			=> 'text',
	                    'description' => __( 'Secret Word set under your 2Checkout -> My Account->Site Management.', 'woocommerce' ),
	                    'default' => '',
	                    'placeholder'	=> ''
				),
				'demo' => array(
						'title' => __( 'Enable Demo Mode', 'woocommerce' ),
						'type' => 'checkbox',
						'label' => __( 'Yes', 'woocommerce' ),
						'default' => 'yes'
				),
				'debug' => array(
                    'title'       => __( 'Debug Log', 'woocommerce' ),
                    'type'        => 'checkbox',
                    'label'       => __( 'Enable logging', 'woocommerce' ),
                    'default'     => 'no',
                    'description' => sprintf( __( 'Log 2Checkout events <em>%s</em>', 'woocommerce' ), wc_get_log_file_path( 'twocheckout' ) )
                ),
				
		);
	}
	

	/**
	 * Process the payment and return the result
	 *
	 * @access public
	 * @param int $order_id
	 * @return array
	 */
	function process_payment( $order_id ) {

		$order = new WC_Order( $order_id );

		$twoco_args = $this->get_twoco_args( $order );
		// ppom_pa($twoco_args); exit;
		
		$twoco_args = http_build_query( $twoco_args, '', '&' );
		
	
		if ($this->demo){
			$checkout_url =	$this->checkout_url_sandbox.'?'.$twoco_args;
		}else{
			$checkout_url =	$this->checkout_url.'?'.$twoco_args;
		}
		
		return array(
				'result' 	=> 'success',
				'redirect'	=> $checkout_url
		);


	}
    
    
    /**
	 * Get 2Checkout Args for passing to PP
	 *
	 * @access public
	 * @param mixed $order
	 * @return array
	 */
	function get_twoco_args( $order ) {
		global $woocommerce;

		$order_id = $order->get_id();

		$curr_code = get_woocommerce_currency();
		if( twoco_convertion_is_on() ) {
			$curr_code = twoco_get_conversion_currency();
		}
		
		// 2Checkout Args
		$twoco_args = array(
				'sid' 					=> $this->seller_id,
				'mode' 					=> '2CO',
				'merchant_order_id'		=> $order_id,
				'lang'					=> get_locale(),
				'currency_code'			=> $curr_code,
					
				// Billing Address info
				'first_name'			=> $order->get_billing_first_name(),
				'last_name'				=> $order->get_billing_last_name(),
				'street_address'		=> $order->get_billing_address_1(),
				'street_address2'		=> $order->get_billing_address_2(),
				'city'					=> $order->get_billing_city(),
				'state'					=> $order->get_billing_state(),
				'zip'					=> $order->get_billing_postcode(),
				'country'				=> $order->get_billing_country(),
				'email'					=> $order->get_billing_email(),
				'phone'					=> $order->get_billing_phone(),
		);

		// Shipping
		
		if ($order->needs_shipping_address()) {

			$twoco_args['ship_name']			= $order->get_shipping_first_name().' '.$order->get_shipping_last_name();
			$twoco_args['company']				= $order->get_shipping_company();
			$twoco_args['ship_street_address']	= $order->get_shipping_address_1();
			$twoco_args['ship_street_address2']	= $order->get_shipping_address_2();
			$twoco_args['ship_city']			= $order->get_shipping_city();
			$twoco_args['ship_state']			= $order->get_shipping_state();
			$twoco_args['ship_zip']				= $order->get_shipping_postcode();
			$twoco_args['ship_country']			= $order->get_shipping_country();
		}
			
		
		$twoco_args['x_receipt_link_url'] 	= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_NM_TwoCheckout', home_url( '/' ) ) );
		$twoco_args['return_url']			= str_replace('https', 'http', $order->get_cancel_order_url());
		
		
		//setting purchas step
		// if all required filled it will skip other steps
		$twoco_args['purchase_step'] = 'payment-method';
		
		
		$twoco_args['paypal_direct'] = 'Y';
		
		$item_names = array();

		if ( sizeof( $order->get_items() ) > 0 ){
			
			$twoco_product_index = 0;
			
			foreach ( $order->get_items() as $item ){
				if ( $item['qty'] )
					$item_names[] = $item['name'] . ' x ' . $item['qty'];
			
				/**
				 * since version 1.6
				 * adding support for both WC Versions
				 */
				$_sku = '';
				if ( function_exists( 'get_product' ) ) {
						
					// Version 2.0
					$product = $order->get_product_from_item($item);
						
					// Get SKU or product id
					if ( $product->get_sku() ) {
						$_sku = $product->get_sku();
					} else {
						$_sku = $product->get_id();
					}
						
				} else {
						
					// Version 1.6.6
					$product = new WC_Product( $item['id'] );
						
					// Get SKU or product id
					if ( $product->get_sku() ) {
						$_sku = $product->get_sku();
					} else {
						$_sku = $item['id'];
					}	
				}
				
				$tangible = "N";
				
				$item_formatted_name 	= $item['name'] . ' (Product SKU: '.$item['product_id'].')';
			
				$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
				$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Order %s' , 'twoco'), $order->get_order_number() ) . " - " . $item_formatted_name;
				$twoco_args['li_'.$twoco_product_index.'_quantity'] = $item['qty'];
				$twoco_args['li_'.$twoco_product_index.'_price'] 	= twoco_get_price($order->get_item_total( $item ));
				$twoco_args['li_'.$twoco_product_index.'_product_id'] = $_sku;
				$twoco_args['li_'.$twoco_product_index.'_tangible'] = $tangible;
				
				$twoco_product_index++;
			}
			
			//getting extra fees since version 2.0+
			$extrafee = $order -> get_fees();
			if($extrafee){
			
				
				$fee_index = 1;
				foreach ( $order -> get_fees() as $item ) {
					
					$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
					$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Other Fee %s' , 'twoco'), $item['name'] );
					$twoco_args['li_'.$twoco_product_index.'_quantity'] = 1;
					$twoco_args['li_'.$twoco_product_index.'_price'] 	= twoco_get_price( $item['line_total'] );

					$fee_index++;
					$twoco_product_index++;
 				}	
			}
			
			// Shipping Cost
			if ( $order -> get_total_shipping() > 0 ) {
				
				
				$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'shipping';
				$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Shipping charges', 'twoco' );
				$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
				$twoco_args['li_'.$twoco_product_index.'_price'] 		= twoco_get_price( $order -> get_total_shipping() );
				// $twoco_args['li_'.$twoco_product_index.'_tangible'] = 'Y';
				
				$twoco_product_index++;
			}
			
			// Taxes (shipping tax too)
			if ( $order -> get_total_tax() > 0 ) {
			
				$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'tax';
				$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Tax', 'twoco' );
				$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
				$twoco_args['li_'.$twoco_product_index.'_price'] 		= twoco_get_price( $order->get_total_tax() );
				
				$twoco_product_index++;
			}

			
		}
		
		$twoco_args = apply_filters( 'woocommerce_twoco_args', $twoco_args );
		
		return $twoco_args;
	}


	/**
	 * Check for 2Checkout IPN Response
	 *
	 * @access public
	 * @return void
	 */
	function twocheckout_response() {
	
		/**
		 * source code: https://github.com/craigchristenson/woocommerce-2checkout-api
		 * Thanks to: https://github.com/craigchristenson
		 */
		global $woocommerce;
		
		// twoco_pa($_REQUEST);
		
		$this->log(__("== INS Response Received == ", "2checkout") );
		$this->log( $_REQUEST );
		
		$wc_order_id = '';
		
		if( !isset($_REQUEST['merchant_order_id']) ) {
			$this->log( '===== NO ORDER NUMBER FOUND =====' );
			exit;
		} 
		
		$wc_order_id = $_REQUEST['merchant_order_id'];
		$this->log(" ==== ORDER -> {$wc_order_id} ====");
		
		
		// echo $wc_order_id;
		$wc_order_id = apply_filters('twoco_order_no_received', $wc_order_id, $_REQUEST);
		$this->log( "Order Received ==> {$wc_order_id}" );
		// exit;
		
		
		$wc_order 		= new WC_Order( absint( $wc_order_id ) );
		
		$this->log("WC API ==> ".$_GET['wc-api']);
		// If redirect after payment
		if( isset($_GET['key']) && (isset($_GET['wc-api']) && strtolower($_GET['wc-api']) == 'wc_gateway_nm_twocheckout') )  {
			
			$this->verify_order_by_hash();
			exit;
		}
		
		$message_type	= isset($_REQUEST['message_type']) ? $_REQUEST['message_type'] : '';
		$sale_id		= isset($_REQUEST['sale_id']) ? $_REQUEST['sale_id'] : '';
		$invoice_id		= isset($_REQUEST['invoice_id']) ? $_REQUEST['invoice_id'] : '';
		$fraud_status	= isset($_REQUEST['fraud_status']) ? $_REQUEST['fraud_status'] : '';
		
		$this->log( "Message Type/Fraud Status: {$message_type}/{$fraud_status}" );
		
		switch( $message_type ) {
			
			case 'ORDER_CREATED':
				$wc_order->add_order_note( sprintf(__('ORDER_CREATED with Sale ID: %d', 'twoco'), $sale_id) );
				$this->log(sprintf(__('ORDER_CREATED with Sale ID: %d', 'twoco'), $sale_id));
				
			break;
			
			case 'FRAUD_STATUS_CHANGED':
				if( $fraud_status == 'pass' ) {
					// Mark order complete
					$wc_order->payment_complete();
					$wc_order->add_order_note( sprintf(__('Payment Status Clear with Invoice ID: %d', 'twoco'), $invoice_id) );
					$this->log(sprintf(__('Payment Status Clear with Invoice ID: %d', 'twoco'), $invoice_id));
					add_action('twoco_order_completed', $order, $sale_id, $invoice_id);
					
				} elseif( $fraud_status == 'fail' ) {
					
					$wc_order->update_status('failed');
					$wc_order->add_order_note(  __("Payment Decliented", 'twoco') );
					$this->log( __("Payment Decliented", 'twoco') );
				}
				
			break;
		}
		
		exit;
	}
	
	
	function verify_order_by_hash() {

			global $woocommerce;
			
			@ob_clean();
			
			$wc_order_id 	= $_REQUEST['merchant_order_id'];
			$order_total	= isset($_REQUEST['total']) ? $_REQUEST['total'] : '';
			$twoco_order_no	= $_REQUEST['order_number'];
			
			if ( isset($_REQUEST['demo']) && $_REQUEST['demo'] == 'Y' ){
				$compare_string = $this->secret_word . $this->seller_id . "1" . $order_total;
			}else{
				$compare_string = $this->secret_word . $this->seller_id . $twoco_order_no . $order_total;
			}
			
			
			$compare_hash1 = strtoupper(md5($compare_string));
			
	
			$compare_hash2 = $_REQUEST['key'];
			if ($compare_hash1 != $compare_hash2) {
				wp_die( "2Checkout Hash Mismatch... check your secret word." );
			} else {
				$wc_order 	= wc_get_order( $wc_order_id );
				$wc_order->add_order_note( sprintf(__('Payment completed via 2Checkout Order Number %d', 'twoco'), $twoco_order_no) );
				// Mark order complete
				$wc_order->payment_complete();
				// Empty cart and clear session
				$woocommerce->cart->empty_cart();
				$order_redirect = add_query_arg('twoco','processed', $this->get_return_url( $wc_order ));
				wp_redirect( $order_redirect );
				exit;
			}
		}

}