<?php
/**
 * Helper functions
 **/
 
if( ! defined( "ABSPATH") ) die("Not Allowed");

function twoco_pa( $array ) {
		echo '<pre>'; print_r($array); echo '</pre>';
}

// Getting rate via free.currencyconverterapi.com
function twoco_get_rates_from_ccapi($amount, $from, $to, $curr_converter_api) {
	
	$conv_id 		= "{$from}_{$to}";
	$endpoint_url	= twoco_get_curr_endpoint();
	$req_url		= "https://{$endpoint_url}/api/v3/convert?q=$conv_id&compact=ultra&apiKey={$curr_converter_api}";
	$response 		= wp_remote_get($req_url);

	if( is_wp_error( $response ) ) {
		return false; // Bail early
	}
	
	if( wp_remote_retrieve_response_code($response) == 400 ) {
		return false;
	}
	
	// twoco_pa($response);

	$body = wp_remote_retrieve_body( $response );
	$response_json	= json_decode($body, true);

	
	return $amount * $response_json[$conv_id];
}

function twoco_get_curr_endpoint() {
	$curr_version = get_option('towco_curr_version');
	
	$endpoint_url = "";
	switch( $curr_version ) {
		
		case 'pro':
			$endpoint_url = "api.currencyconverterapi.com";
		break;
		case 'prepaid':
			$endpoint_url = "prepaid.currconv.com";
		break;
		default:
			$endpoint_url = "free.currencyconverterapi.com";
		break;
			
	}
	
	return $endpoint_url;
}

//getting rates form Yahoo
function twoco_get_rates_from_yahoo() {

	// set the host name variable and get the file
	$nm_date_to_get = date("Ydm",time()-86400);
	$nm_ajax_host = 'http://finance.yahoo.com/connection/currency-converter-cache?date='.$nm_date_to_get.'&output=json';
	$nm_ajax_data = wp_remote_get($nm_ajax_host);
	
	
	var_dump($nm_ajax_host); exit;
	
	// if it returns an error, then lets get out of here.
	if (is_wp_error($nm_ajax_data)) {
		return;
	}
	
	// grab the body, remove the JSONP wrapper, then decode it
	$nm_ajax_data = $nm_ajax_data['body'];
	$nm_ajax_data = str_replace("/**/YAHOO.Finance.CurrencyConverter.addConversionRates(",'',$nm_ajax_data);
	
	
	$nm_ajax_data = substr($nm_ajax_data,0,strrpos($nm_ajax_data,');'));
	//var_dump($nm_ajax_data);
	$nm_ajax_data = json_decode($nm_ajax_data,true);

	
	// if the output of json_decode isn't an array, let's get out of here.
	if (!is_array($nm_ajax_data)) {
		return;
	}

	// create an array to return
	$nm_array_to_return = array();
	foreach ($nm_ajax_data['list']['resources'] as $key => $value) {
		$nm_array_to_return[substr($value['resource']['fields']['symbol'],0,3)] = $value['resource']['fields']['price'];
	}
	
	return $nm_array_to_return;
}

function twoco_is_currency_supported() {
  $supported_currencies = array(
    'AFN', 'ALL', 'DZD', 'ARS', 'AUD', 'AZN', 'BSD', 'BDT', 'BBD',
    'BZD', 'BMD', 'BOB', 'BWP', 'BRL', 'GBP', 'BND', 'BGN', 'CAD', 
    'CLP', 'CNY', 'COP', 'CRC', 'HRK', 'CZK', 'DKK', 'DOP', 'XCD', 
    'EGP', 'EUR', 'FJD', 'GTQ', 'HKD', 'HNL', 'HUF', 'INR', 'IDR', 
    'ILS', 'JMD', 'JPY', 'KZT', 'KES', 'LAK', 'MMK', 'LBP', 'LRD', 
    'MOP', 'MYR', 'MVR', 'MRO', 'MUR', 'MXN', 'MAD', 'NPR', 'TWD', 
    'NZD', 'NIO', 'NOK', 'PKR', 'PGK', 'PEN', 'PHP', 'PLN', 'QAR', 
    'RON', 'RUB', 'WST', 'SAR', 'SCR', 'SGD', 'SBD', 'ZAR', 'KRW', 
    'LKR', 'SEK', 'CHF', 'SYP', 'THB', 'TOP', 'TTD', 'TRY', 'UAH', 
    'AED', 'USD', 'VUV', 'VND', 'XOF', 'YER');
    if ( ! in_array( get_woocommerce_currency(), apply_filters( 'twocheckout_supported_currencies', $supported_currencies ) ) ) return false;
    return true;
}

function twoco_convertion_is_on() {
	
	// $enabled = false;
	/*$xchange_rate 		= get_option('xchange_rate');
	$curr_converter_api	= trim( get_option('curr_converter_api') );
	if( $xchange_rate == 'yes' && $curr_converter_api != '' ) {
		$enabled = true;
	}*/
	
	$curr_updatable		= get_option('twoco_currency_updated');
	if( $curr_updatable ) {
		
		$enabled = true;
	}
	
	return $enabled;
}

function twoco_get_price($price){
	
	if( twoco_is_currency_supported() ) {
		return $price;
	}
	
	if( twoco_convertion_is_on() ){
		$curr_converter_api	= trim( get_option('curr_converter_api') );
		$price = twoco_get_converted_price( $price, $curr_converter_api );
	}
	
	$price = wc_format_decimal($price, wc_get_price_decimals());
	
	return apply_filters('twoco_xchanged_price', $price);
}

function twoco_get_converted_price( $price, $curr_converter_api ) {
	
	$from_curr	= get_woocommerce_currency();
	$to_curr	= twoco_get_conversion_currency();
	
	return twoco_get_rates_from_ccapi($price, $from_curr, $to_curr, $curr_converter_api);
}
// Return label for Currency Label with current rate
function twoco_currency_label( $xchange_rate, $curr_converter_api ) {
	
	if( $xchange_rate != 'yes' ) return '';
	
	if( $curr_converter_api == '' ) return '';
	
	if( ! is_admin() ) return '';
	
	$from_curr	= get_woocommerce_currency();
	
	$label = 'Enable';
	if( ! twoco_is_currency_supported() ) {
		
		$label .= " - {$from_curr} No Supported";
	}
	
	$amount = 1;
	
	$price_converted = twoco_get_converted_price( $amount, $curr_converter_api);
	if( $price_converted === false ) {
		update_option('twoco_currency_updated', false);
		$label = "<div class='notice notice-error'>Failed to update rate, make sure API key is correct</div>";
	} else {
		
		update_option('twoco_currency_updated', true);
		$conversion_rate = wc_format_decimal( $price_converted, 4 );
		$to_curr		= twoco_get_conversion_currency();
		$label .= " - ({$amount} {$from_curr} = {$conversion_rate} {$to_curr})";
	}
	
	
	
	return $label;
}

// Get converted currency
function twoco_get_conversion_currency() {
	
	return apply_filters('twoco_conversion_currency', 'USD');
}

function twoco_log( $log ) {
	
	if ( true === true ) {
      if ( is_array( $log ) || is_object( $log ) ) {
          $resp = error_log( print_r( $log, true ), 3, plugin_dir_path(__FILE__).'twoco.log' );
      } else {
          $resp = error_log( $log, 3, plugin_dir_path(__FILE__).'twoco.log' );
      }
      
  }
}

// PPOM Settings
function twoco_get_settings() {
	
	$the_link 	= 'https://najeebmedia.com/';
	$more_detail = '<a target="_blank" href="'.esc_attr($the_link).'">What is Mode?<a>';
	$paypal_direct_url = '<a target="_blank" href="'.esc_attr($the_link).'">What is Direct PayPal?<a>';
	$inline_display_url = '<a target="_blank" href="'.esc_attr($the_link).'">What is Payment Display?<a>';
	$api_key_url	 = 'https://free.currencyconverterapi.com/free-api-key';
	$get_apikey_url	= '<a target="_blank" href="'.esc_url($api_key_url).'">';
	
	$xchange_rate 		= get_option('xchange_rate');
	$curr_converter_api	= trim( get_option('curr_converter_api') );
	
	if( ! twoco_is_currency_supported() && $curr_converter_api != '') {
		$xchange_rate = 'yes';
	}
	
	$xchange_rate_desc = sprintf(__("If your currency is not supported by 2CO, then use this option. It will automatically convert your currency to %s using API",'twoco'),twoco_get_conversion_currency());
	$xchange_api_desc  = sprintf(__("Get API Key to use Currency Converter. %s Get API Key.",'twoco'), $get_apikey_url);
	
	$twoco_settings = array(
       
		array(
			'title' => '2Checkout Payment Gateway Settings',
			'type'  => 'title',
			'desc'	=> __("2Checkout Payment Gateway Global Settings.", 'twoco'),
			'id'    => 'twoco_labels_settings',
		),
		
		array(
				'name'    => __( "Select Payment Mode", 'twoco' ),
		    'desc'    => $more_detail,
		    'id'      => 'towco_payment_mode',
		    'css'     => 'min-width:150px;',
		    'std'     => 'twoco_cc_form', // WooCommerce < 2.0
		    'default' => 'twoco_cc_form', // WooCommerce >= 2.0
		    'type'    => 'select',
		    'options' => array(
		      'twoco_cc_form'       => __( 'Credit Card Form - On Checkout', 'twoco' ),
		      'convert_plus'        => __( 'ConvertPlus Inline PopUp - Thanks Page', 'twoco' ),
		      'legacy_inline'       => __( 'Legacy Inline PopUp - Thanks Page', 'twoco' ),
		      'standard_checkout'		=> __( 'Standard Checkout - Redirected to 2Checkout', 'twoco'),
		    ),
		    'desc_tip' =>  false,
		    ),
		array(
				'name'    => __( "Inline Display", 'twoco' ),
		    'desc'    => $inline_display_url,
		    'id'      => 'towco_inline_display',
		    'css'     => 'min-width:150px;',
		    'std'     => 'auto_popup', // WooCommerce < 2.0
		    'default' => 'auto_popup', // WooCommerce >= 2.0
		    'type'    => 'select',
		    'options' => array(
		      'auto_popup'        => __( 'Auto Popup', 'twoco' ),
		      'show_button'       => __( 'Show Button', 'twoco' ),
		    ),
		    'desc_tip' =>  false,
		    ),
		array(
				'title'          => __( 'Enable PayPal Direct?', 'twoco' ),
				'type'          => 'checkbox',
				'label'         => __( 'Yes', 'twoco' ),
				'default'       => 'no',
				'id'            => 'twoco_paypal_direct',
				'desc'          => __( 'Yes. '.$paypal_direct_url, 'twoco' ),
			),
		array(
				'title'          => __( 'Enable Currency Conversion?', 'twoco' ),
				'type'          => 'checkbox',
				'default'       => 'no',
				'id'            => 'xchange_rate',
				'desc'          => $xchange_rate_desc.' '.twoco_currency_label($xchange_rate, $curr_converter_api),
			),
		array(
				'title'          => __( 'Currency Conversion API', 'twoco' ),
				'type'          => 'text',
				'default'       => '',
				'id'            => 'curr_converter_api',
				'desc'          => $xchange_api_desc,
			),
        array(
				'name'    => __( "Currency Conversion Version", 'twoco' ),
			    'desc'    => $more_detail,
			    'id'      => 'towco_curr_version',
			    'css'     => 'min-width:150px;',
			    'std'     => 'free', // WooCommerce < 2.0
			    'default' => 'free', // WooCommerce >= 2.0
			    'type'    => 'select',
			    'options' => array(
			      'free'       => __( 'Free Version', 'twoco' ),
			      'pro'        => __( 'Premium', 'twoco' ),
			      'prepaid'       => __( 'Pre-paid', 'twoco' ),
			    ),
			    'desc_tip' =>  false,
			    ),
	    array(
			'type' => 'sectionend',
			'id'   => 'twoco_labels_settings',
		),
			
        
		);
		
	return apply_filters('twoco_settings_data', $twoco_settings);
}