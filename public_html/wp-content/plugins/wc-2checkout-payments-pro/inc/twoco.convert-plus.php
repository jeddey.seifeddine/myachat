<?php
/**
 * 2Checkout payment gateway class
 **/
 
 if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

	class WC_Gateway_NM_TwoCheckout extends WC_Payment_Gateway {

		// Logging
        public static $log_enabled = false;
        public static $log = false;
        
		var $seller_id;
		var $demo;
		var $plugin_url;

		public function __construct(){
			
			global $woocommerce;

			$this -> plugin_url = untrailingslashit(plugin_dir_url( __FILE__ ));
			
			$this->converted_curr		= twoco_get_conversion_currency();
			$this->id 					= 'nmwoo_2co';
			$this->has_fields   		= false;
			$this->checkout_url     	= 'https://www.2checkout.com/checkout/purchase';
			$this->checkout_url_sandbox	= 'https://sandbox.2checkout.com/checkout/purchase';
			$icon_url					= ($this->get_option( 'image' ) != '' ? $this->get_option( 'image' ) : TWOCO_URL.'/images/2co_logo.png');
			$this->icon 				= $icon_url;
			$this->method_title 		= '2Checkout - Credit Card/PayPal';
			$this->method_description 	= 'This plugin add 2checkout payment gateway with Woocommerce based shop. Make sure you have set your 2co account according <a href="http://najeebmedia.com/2checkout-payment-gateway-for-woocommerce/" target="_blank">these setting</a>';
			
			// Publishable/Public Keys
			$this->publishable_key		= $this->get_option( 'publishable_key' );
			$this->private_key			= $this->get_option( 'private_key' );
			
			$this->inline				= $this->get_option( 'inline' );
			$this->title 				= $this->get_option( 'title' );
			$this->description 			= $this->get_option( 'description' );
			$this->seller_id			= $this->get_option( 'seller_id' );
			$this->secret_word			= trim($this->get_option( 'secret_word' ));
			$this->demo 				= $this->get_option('demo');
			$this->debug 				= $this->get_option('debug');
			$this->cc_form 				= $this->get_option('cc_form');
			$this->paypal_direct 		= $this->get_option('paypal_direct'); 
			// Free API key
			
			$this->xchange_rate 		= $this->get_option('xchange_rate');
			$this->curr_converter_api	= trim( $this->get_option('curr_converter_api') );
			
			if( ! twoco_is_currency_supported() && $this->curr_converter_api != '') {
				$this->xchange_rate = 'yes';
			}
				
			$this->demo					= $this->demo == 'yes' ? true : false;
				
			$this->init_form_fields();
			$this->init_settings();
			
			self::$log_enabled = $this->debug;
				
			// Save options
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			//add_action('process_2co_ipn_request', array( $this, 'successful_request' ), 1 );
			
			// Payment listener/API hook
			add_action( 'woocommerce_api_wc_gateway_nm_twocheckout', array( $this, 'twocheckout_response' ) );
			
			// We need custom JavaScript to obtain a token
			// add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
			
			// add_action('wp_head', array($this, 'twoco_convert_plus_script'));
			
			
			add_action( 'woocommerce_thankyou', array($this, 'init_twoco_inline_payment'), 10, 1 );
			
		}
		
		function init_twoco_inline_payment( $order_id ) {
			
			if( ! $this->inline_checkout_on() ) return;
			
			// twoCO Library init
			$this->twoco_convert_plus_script();
			
			$order = new WC_Order( $order_id );
			// var_dump($order->get_payment_method()); exit;
			
			// If other payment method is enabled
			if( $order->get_payment_method() != 'nmwoo_2co' ) return;
			
			
			$order_data 				= array();
        	$order_data['email']		= $order->get_billing_email();
        	$order_data['full_name']	= $order->get_billing_first_name().' '.$order->get_billing_last_name();
        	$order_data['country']		= $order->get_billing_country();
        	$order_data['order_amount']	= $order->get_total();
        	$order_data['order_id'] 	= $order_id;
        	$order_data['redirect']		= $this->get_return_url( $order );
        	
        	
        	if( get_option('towco_inline_display') == 'auto_popup' ){
				$this->inline_checkout_script_popup($order_data);
			} else {
				$this->inline_checkout_script_button($order_data);
			}
				
		}
		
		
		function twoco_convert_plus_script() {
			
			// if( ! is_wc_endpoint_url( 'order-receiveds' ) ) return;
			
			?>
			<script>
			var merchant_code = "<?php echo $this->seller_id;?>";
    		console.log(merchant_code);
		    (function (document, src, libName, config) {
		        var script             = document.createElement('script');
		        script.src             = src;
		        script.async           = true;
		        var firstScriptElement = document.getElementsByTagName('script')[0];
		        script.onload          = function () {
		            for (var namespace in config) {
		                if (config.hasOwnProperty(namespace)) {
		                    window[libName].setup.setConfig(namespace, config[namespace]);
		                }
		            }
		            window[libName].register();
		            
		            /*TwoCoInlineCart.events.subscribe('cart:closed', function () {
 	    
 	    				console.log('payment done');
				        console.log(TwoCoInlineCart.cart.getSuccessURL());
				    });*/
		        };
		        
		        // 250222256171 - BestCottonBedSheet
        		// 250146291686 - PestCart
        		firstScriptElement.parentNode.insertBefore(script, firstScriptElement);
		    })(document, 'https://secure.avangate.com/checkout/client/twoCoInlineCart.js', 'TwoCoInlineCart',{"app":{"merchant":merchant_code},"cart":{"host":"https:\/\/secure.2checkout.com","customization":"inline"}});
		</script>
		
		<?php
		}
		
		function inline_checkout_script_popup($order_data) {
			
			// if payment processed/redirected
			if( isset($_GET['refno']) ) return;
			
			if( ! is_wc_endpoint_url( 'order-received' ) ) return;
			
			global $wp;

		  	wp_register_script( 'woocommerce_twoco', TWOCO_URL.'/js/twoco-convert-plus.js', array( 'jquery' ), 8.0, true );
			
			// in most payment processors you have to use PUBLIC KEY to obtain a token
			wp_localize_script( 'woocommerce_twoco', 'twoco_vars', array(
				'is_demo'			=> $this->demo,
				'seller_id'			=> $this->seller_id,
				'checkout_title'	=> apply_filters('twoco_checkout_title', get_bloginfo('name')),
				'wc_currency'		=> get_woocommerce_currency(),
				'wp_lang'			=> get_bloginfo("language"),
				'error_message'		=> __('Please check your Seller ID, Publishable or Private Key', 'twoco'),
				'order_data'		=> $order_data,
				'inline_display'	=> get_option('towco_inline_display'),
			) );
		 
			wp_enqueue_script( 'woocommerce_twoco' );
			
		}
		
		function inline_checkout_script_button($order_data) {
			
			// if payment processed/redirected
			if( isset($_GET['refno']) ) return;
			
			if( ! is_wc_endpoint_url( 'order-received' ) ) return;
			
			global $wp;
			
			
			echo '<div id="twoco-inline-form">';
			echo '<div class="check_out_btn">';
			echo "<input id='twco-btn-form' name='submit' class='button alt' type='submit' value='".esc_attr($this->get_option('title'))."' />";
			echo '</div>';
			echo '</div>';
			
		  	wp_register_script( 'woocommerce_twoco', TWOCO_URL.'/js/twoco-convert-plus.js', array( 'jquery' ), 8.0, true );
		  	wp_enqueue_script( 'twoco-inline','//www.2checkout.com/static/checkout/javascript/direct.min.js', array( 'jquery' ),'',true );
			wp_enqueue_style( 'twoco-inline-css', TWOCO_URL.'/checkout-model.css');
			
			// in most payment processors you have to use PUBLIC KEY to obtain a token
			wp_localize_script( 'woocommerce_twoco', 'twoco_vars', array(
				'is_demo'			=> $this->demo,
				'seller_id'			=> $this->seller_id,
				'checkout_title'	=> apply_filters('twoco_checkout_title', get_bloginfo('name')),
				'wc_currency'		=> get_woocommerce_currency(),
				'wp_lang'			=> get_bloginfo("language"),
				'error_message'		=> __('Please check your Seller ID, Publishable or Private Key', 'twoco'),
				'order_data'		=> $order_data,
				'inline_display'	=> get_option('towco_inline_display'),
			) );
		 
			wp_enqueue_script( 'woocommerce_twoco' );
			
		}


		function init_form_fields(){

			$currency_symbol = get_woocommerce_currency();
			$from_curr		 = $this->converted_curr;
			$api_key_url	 = 'https://free.currencyconverterapi.com/free-api-key';

			$this->form_fields = array(
					'enabled' => array(
							'title' => __( 'Enable 2CO', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Yes', 'woocommerce' ),
							'default' => 'yes'
					),
					'seller_id' => array(
							'title' => __( '2CO Account #', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'This Seller ID issued by 2Checkout', 'woocommerce' ),
							'default' => '',
							'desc_tip'      => true,
					),
					'title' => array(
							'title' => __( 'Title', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
							'default' => __( '2Checkout Payment', 'woocommerce' ),
							'desc_tip'      => true,
					),
					'description' => array(
							'title' => __( 'Customer Message', 'woocommerce' ),
							'type' => 'textarea',
							'default' => 'Please enter valid credit cart detail.',
					),
					'image' => array(
							'title' => __( 'Image URL', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'Use your own image at checkout page by pasting image URL from Media Library. Left blank for default.', 'woocommerce' ),
							'default' => __( '', 'woocommerce' ),
							'desc_tip'      => true,
					),
					'demo' => array(
							'title' => __( 'Enable Demo Mode', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Yes', 'woocommerce' ),
							'default' => 'yes'
					),
					'debug' => array(
                            'title'       => __( 'Debug Log', 'woocommerce' ),
                            'type'        => 'checkbox',
                            'label'       => __( 'Enable logging', 'woocommerce' ),
                            'default'     => 'no',
                            'description' => sprintf( __( 'Log 2Checkout events <em>%s</em>', 'woocommerce' ), wc_get_log_file_path( 'twocheckout' ) )
                        ),
                        
					// 'xchange_rate' => array(
					// 		'title' => __( 'Currency Converter', 'woocommerce' ),
					// 		'type' => 'checkbox',
					// 		'label' => twoco_currency_label($this->xchange_rate, $this->curr_converter_api),
					// 		'default' => 'no',
					// 		'description' => __( 'If your currency is not supported by 2CO, then use this option. It will automatically convert your currency to '.$this->converted_curr.' using API', 'woocommerce' ),
					// 		'desc_tip'      => false,
					// ),
					// 'curr_converter_api' => array(
					// 		'title' => __( 'Currency Converter API', 'woocommerce' ),
					// 		'type' => 'text',
					// 		'description' => __( 'Get API Key to use Currency Converter. <a target="_blank" href="'.esc_url($api_key_url).'">Get API Key.', 'woocommerce' ),
					// 		'desc_tip'      => false,
					// ),
			);
		}
		
	
		/**
        * Logging method
        * @param  string $message
        */
        public static function log( $message ) {
	        if ( self::$log_enabled ) {
	            if ( empty( self::$log ) ) {
	                self::$log = new WC_Logger();
	            }
	            
	            $message = is_array($message) ? json_encode($message) : $message;
	            self::$log->add( 'twocheckout', $message );
	        }
	    }
        
        
        /**
		 * Process the payment and return the result
		 *
		 * @access public
		 * @param int $order_id
		 * @return array
		 */
		function process_payment( $order_id ) {

			$order = new WC_Order( $order_id );
			
			// $this->demo = true;
			
			$twoco_args = $this->get_twoco_args( $order );
			// ppom_pa($twoco_args); exit;
			
			$twoco_args = http_build_query( $twoco_args, '', '&' );
			
			//if demo is enabled
			$checkout_url = '';
			
			if( $this->inline_checkout_on() ) {
				$checkout_url = $this->get_return_url( $order );
			} else {
				
				if ($this->demo){
					$checkout_url =	$this->checkout_url_sandbox;
				}else{
					$checkout_url =	$this->checkout_url;
				}
				
				$checkout_url = $checkout_url.'?'.$twoco_args;
			}
			
			
			return array(
					'result' 	=> 'success',
					'redirect'	=> $checkout_url
			);


		}
        
        
        /**
		 * Get 2Checkout Args for passing to PP
		 *
		 * @access public
		 * @param mixed $order
		 * @return array
		 */
		function get_twoco_args( $order ) {
			global $woocommerce;

			$order_id = $order->get_id();

			$curr_code = get_woocommerce_currency();
			if( $this->convertion_is_on() ) {
				$curr_code = $this->converted_curr;
			}
			
			// 2Checkout Args
			$twoco_args = array(
					'sid' 					=> $this->seller_id,
					'mode' 					=> '2CO',
					'merchant_order_id'		=> $order_id,
					'currency_code'			=> $curr_code,
						
					// Billing Address info
					'first_name'			=> $order->billing_first_name,
					'last_name'				=> $order->billing_last_name,
					'street_address'		=> $order->billing_address_1,
					'street_address2'		=> $order->billing_address_2,
					'city'					=> $order->billing_city,
					'state'					=> $order->billing_state,
					'zip'					=> $order->billing_postcode,
					'country'				=> $order->billing_country,
					'email'					=> $order->billing_email,
					'phone'					=> $order->billing_phone,
			);

			// Shipping
			
			if ($order->needs_shipping_address()) {

				$twoco_args['ship_name']			= $order->shipping_first_name.' '.$order->shipping_last_name;
				$twoco_args['company']				= $order->shipping_company;
				$twoco_args['ship_street_address']	= $order->shipping_address_1;
				$twoco_args['ship_street_address2']	= $order->shipping_address_2;
				$twoco_args['ship_city']			= $order->shipping_city;
				$twoco_args['ship_state']			= $order->shipping_state;
				$twoco_args['ship_zip']				= $order->shipping_postcode;
				$twoco_args['ship_country']			= $order->shipping_country;
			}
				
			
			$twoco_args['x_receipt_link_url'] 	= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_NM_TwoCheckout', home_url( '/' ) ) );
			$twoco_args['return_url']			= str_replace('https', 'http', $order->get_cancel_order_url());
			
			
			//setting purchas step
			// if all required filled it will skip other steps
			$twoco_args['purchase_step'] = 'payment-method';
			
			
			//if demo is enabled
			if ($this->demo){
				$twoco_args['demo'] =	'Y';
			}
			
			if( !$this->demo && $this->paypal_direct == 'yes' ) {
				$twoco_args['paypal_direct'] = 'Y';
			}

			$item_names = array();

			if ( sizeof( $order->get_items() ) > 0 ){
				
				$twoco_product_index = 0;
				
				foreach ( $order->get_items() as $item ){
					if ( $item['qty'] )
						$item_names[] = $item['name'] . ' x ' . $item['qty'];
				
					/**
					 * since version 1.6
					 * adding support for both WC Versions
					 */
					$_sku = '';
					if ( function_exists( 'get_product' ) ) {
							
						// Version 2.0
						$product = $order->get_product_from_item($item);
							
						// Get SKU or product id
						if ( $product->get_sku() ) {
							$_sku = $product->get_sku();
						} else {
							$_sku = $product->get_id();
						}
							
					} else {
							
						// Version 1.6.6
						$product = new WC_Product( $item['id'] );
							
						// Get SKU or product id
						if ( $product->get_sku() ) {
							$_sku = $product->get_sku();
						} else {
							$_sku = $item['id'];
						}	
					}
					
					$tangible = "N";
					
					$item_formatted_name 	= $item['name'] . ' (Product SKU: '.$item['product_id'].')';
				
					$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
					$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Order %s' , 'woocommerce'), $order->get_order_number() ) . " - " . $item_formatted_name;
					$twoco_args['li_'.$twoco_product_index.'_quantity'] = $item['qty'];
					$twoco_args['li_'.$twoco_product_index.'_price'] 	= $this->get_price($order->get_item_total( $item ));
					$twoco_args['li_'.$twoco_product_index.'_product_id'] = $_sku;
					$twoco_args['li_'.$twoco_product_index.'_tangible'] = $tangible;
					
					$twoco_product_index++;
				}
				
				//getting extra fees since version 2.0+
				$extrafee = $order -> get_fees();
				if($extrafee){
				
					
					$fee_index = 1;
					foreach ( $order -> get_fees() as $item ) {
						
						$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
						$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Other Fee %s' , 'woocommerce'), $item['name'] );
						$twoco_args['li_'.$twoco_product_index.'_quantity'] = 1;
						$twoco_args['li_'.$twoco_product_index.'_price'] 	= $this->get_price( $item['line_total'] );

						$fee_index++;
						$twoco_product_index++;
	 				}	
				}
				
				// Shipping Cost
				if ( $order -> get_total_shipping() > 0 ) {
					
					
					$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'shipping';
					$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Shipping charges', 'woocommerce' );
					$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
					$twoco_args['li_'.$twoco_product_index.'_price'] 		= $this->get_price( $order -> get_total_shipping() );
					// $twoco_args['li_'.$twoco_product_index.'_tangible'] = 'Y';
					
					$twoco_product_index++;
				}
				
				// Taxes (shipping tax too)
				if ( $order -> get_total_tax() > 0 ) {
				
					$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'tax';
					$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Tax', 'woocommerce' );
					$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
					$twoco_args['li_'.$twoco_product_index.'_price'] 		= $this->get_price( $order->get_total_tax() );
					
					$twoco_product_index++;
				}

				
			}
			
			$twoco_args = apply_filters( 'woocommerce_twoco_args', $twoco_args );
			
			return $twoco_args;
		}

	
		/**
		 * Check for 2Checkout IPN Response
		 *
		 * @access public
		 * @return void
		 */
		function twocheckout_response() {
		
			/**
			 * source code: https://github.com/craigchristenson/woocommerce-2checkout-api
			 * Thanks to: https://github.com/craigchristenson
			 */
			global $woocommerce;
			
			$this->log(__("== INS Response Received == ", "2checkout") );
			$this->log( $_REQUEST );
			
			$wc_order_id = '';
			
			if( !isset($_REQUEST['REFNOEXT']) ) {
				$this->log( '===== NO ORDER NUMBER FOUND =====' );
				exit;
			} 
			
			$wc_order_id = $_REQUEST['REFNOEXT'];
			$this->log(" ==== ORDER -> {$wc_order_id} ====");
			
			@ob_clean();
			$wc_order 		= new WC_Order( absint( $wc_order_id ) );
			
			// Check if order payment already completed
			if( $wc_order->has_status( 'processing' ) ) {
				$this->log('===== ORDER PAYMENT COMPLETED ALREADY =====');
				exit;
			}
			
			$order_status	= isset($_REQUEST['ORDERSTATUS']) ? $_REQUEST['ORDERSTATUS'] : '';
			$ref_no			= isset($_REQUEST['REFNO']) ? $_REQUEST['REFNO'] : '';
			$invoice_id		= isset($_REQUEST['invoice_id']) ? $_REQUEST['invoice_id'] : '';
			$fraud_status	= isset($_REQUEST['fraud_status']) ? $_REQUEST['fraud_status'] : '';
			
			switch( $order_status ) {
				
				case 'PAYMENT_AUTHORIZED':
					$wc_order->add_order_note( sprintf(__('PAYMENT_AUTHORIZED with Ref No: %d', 'twoco'), $ref_no) );
					$this->log(sprintf(__('PAYMENT_AUTHORIZED with Ref No: %d', 'twoco'), $ref_no));
					
				break;
				
				case 'COMPLETE':
					// Mark order complete
					$wc_order->payment_complete();
					$wc_order->add_order_note( sprintf(__('Payment Completed with Ref No: %d', 'twoco'), $ref_no) );
					twoco_log(sprintf(__('Payment Status Clear with Ref No: %d', 'twoco'), $invoice_id));
					add_action('twoco_order_completed_convertplus', $wc_order, $ref_no);
					// Empty cart and clear session
					$woocommerce->cart->empty_cart();
				break;
			}
			
			exit;
		}
		
		function get_price($price){
		
			if( $this->convertion_is_on() ){
				
				$price = twoco_get_converted_price( $price, $this->curr_converter_api );
			}
			
			$price = wc_format_decimal($price, wc_get_price_decimals());
			
			return apply_filters('xchanged_price', $price);
		}
		
		
		// Return T|F for credit card fields
		function show_cc_form() {
			
			$show_cc = true;
			
			if ( ! $this->demo && ! is_ssl() ) {
				$show_cc = false;
			}
			
			if( $this->cc_form != 'yes' ) {
				
				$show_cc = false;
			}
			
			return apply_filters('twoco_show_cc_form', $show_cc);
		}
		
		// Return T|F for credit card fields
		function inline_checkout_on() {
			
			$inline_on = true;
			
			if( $this->inline != 'yes' ) {
				
				$inline_on = false;
			}
			
			return apply_filters('twoco_inline_checkout', $inline_on);
		}
		
		function convertion_is_on() {
		
			$enabled = false;
			if( $this->xchange_rate == 'yes' && $this->curr_converter_api != '' ) {
				$enabled = true;
			}
			
			return $enabled;
		}
	}