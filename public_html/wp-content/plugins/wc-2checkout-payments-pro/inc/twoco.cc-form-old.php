<?php

if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;


        class WC_Gateway_NM_TwoCheckout extends WC_Payment_Gateway {

		// Logging
        public static $log_enabled = false;
        public static $log = false;
        
		var $seller_id;
		var $demo;
		var $plugin_url;

		public function __construct(){
			
			global $woocommerce;

			$this -> plugin_url = untrailingslashit(plugin_dir_url( __FILE__ ));
			
			$this->converted_curr		= twoco_get_conversion_currency();
			$this->id 					= 'nmwoo_2co';
			$this->has_fields   		= false;
			$this->checkout_url     	= 'https://www.2checkout.com/checkout/purchase';
			$this->checkout_url_sandbox	= 'https://sandbox.2checkout.com/checkout/purchase';
			$icon_url					= ($this->get_option( 'image' ) != '' ? $this->get_option( 'image' ) : $this -> plugin_url.'/images/2co_logo.png');
			$this->icon 				= $icon_url;
			$this->method_title 		= '2Checkout - Credit Card/PayPal';
			$this->method_description 	= '2Checkout Payment Gateway for WooCommerce. Setup your 2Checkout account with <a href="http://najeebmedia.com/2checkout-payment-gateway-for-woocommerce/" target="_blank">these setting</a>';
            $this->has_fields = true;
			
			// Publishable/Public Keys
			$this->publishable_key		= $this->get_option( 'publishable_key' );
			$this->private_key			= $this->get_option( 'private_key' );
			
			$this->inline				= $this->get_option( 'inline' );
			$this->title 				= $this->get_option( 'title' );
			$this->description 			= $this->get_option( 'description' );
			$this->seller_id			= $this->get_option( 'seller_id' );
			$this->secret_word			= trim($this->get_option( 'secret_word' ));
			$this->demo 				= $this->get_option('demo');
			$this->debug 				= $this->get_option('debug');
			$this->cc_form 				= $this->get_option('cc_form');
			$this->paypal_direct 		= $this->get_option('paypal_direct'); 
			// Free API key
			
			$this->xchange_rate 		= $this->get_option('xchange_rate');
			$this->curr_converter_api	= trim( $this->get_option('curr_converter_api') );
			
			if( ! twoco_is_currency_supported() && $this->curr_converter_api != '') {
				$this->xchange_rate = 'yes';
			}
				
			$this->demo					= $this->demo == 'yes' ? true : false;
				
			$this->init_form_fields();
			$this->init_settings();
			
			self::$log_enabled = $this->debug;
				
			// Save options
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			//add_action('process_2co_ipn_request', array( $this, 'successful_request' ), 1 );
			
			// Payment listener/API hook
			add_action( 'woocommerce_api_wc_gateway_nm_twocheckout', array( $this, 'twocheckout_response' ) );
			
			// We need custom JavaScript to obtain a token
			add_action( 'wp_enqueue_scripts', array( $this, 'payment_scripts' ) );
			
		}
		
		
		function payment_scripts() {
			
			// we need JavaScript to process a token only on cart/checkout pages, right?
			if ( ! is_cart() && ! is_checkout() && ! isset( $_GET['pay_for_order'] ) ) {
				return;
			}
			
			// if our payment gateway is disabled, we do not have to enqueue JS too
			if ( 'no' === $this->enabled ) {
				return;
			}
			
			// no reason to enqueue JavaScript if API keys are not set
			if ( empty( $this->private_key ) || empty( $this->publishable_key ) ) {
				return;
			}
			
			// do not work with card detailes without SSL unless your website is in a test mode
			if ( ! $this->show_cc_form() ) {
				return;
			}
			
			
			// let's suppose it is our payment processor JavaScript that allows to obtain a token
			wp_enqueue_script( 'twoco_js', '//www.2checkout.com/checkout/api/2co.min.js' );
			
			// and this is our custom JS in your plugin directory that works with token.js
			wp_register_script( 'woocommerce_twoco', plugins_url( 'js/twoco.js', __FILE__ ), array( 'jquery', 'twoco_js' ) );
		 
			// in most payment processors you have to use PUBLIC KEY to obtain a token
			wp_localize_script( 'woocommerce_twoco', 'twoco_vars', array(
				'publishable_key'	=> $this->publishable_key,
				'private_key'		=> $this->private_key,
				'is_demo'			=> $this->demo,
				'seller_id'			=> $this->seller_id,
				'error_message'		=> __('Please check your Seller ID, Publishable or Private Key', 'twoco'),
			) );
		 
			wp_enqueue_script( 'woocommerce_twoco' );
		}


		function init_form_fields(){

			$currency_symbol = get_woocommerce_currency();
			$from_curr		 = $this->converted_curr;
			$api_key_url	 = 'https://free.currencyconverterapi.com/free-api-key';

			$this->form_fields = array(
					'enabled' => array(
							'title' => __( 'Enable 2CO', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Yes', 'woocommerce' ),
							'default' => 'yes'
					),
					'seller_id' => array(
							'title' => __( '2CO Account #', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'This Seller ID issued by 2Checkout', 'woocommerce' ),
							'default' => '',
							'desc_tip'      => true,
					),
					'title' => array(
							'title' => __( 'Title', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
							'default' => __( '2Checkout Payment', 'woocommerce' ),
							'desc_tip'      => true,
					),
					'description' => array(
							'title' => __( 'Customer Message', 'woocommerce' ),
							'type' => 'textarea',
							'default' => 'Please enter valid credit cart detail.',
					),
					'image' => array(
							'title' => __( 'Image URL', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'Use your own image at checkout page by pasting image URL from Media Library. Left blank for default.', 'woocommerce' ),
							'default' => __( '', 'woocommerce' ),
							'desc_tip'      => true,
					),
					'publishable_key' => array(
							'title' => __( 'Publishable Key', 'woocommerce' ),
		                    'type' 			=> 'text',
		                    'description' => __( 'Please enter your 2Checkout Publishable Key.', 'woocommerce' ),
		                    'default' => '',
		                    'desc_tip'      => true,
		                    'placeholder'	=> ''
					),
					
					'private_key' => array(
							'title' => __( 'Private Key', 'woocommerce' ),
		                    'type' 			=> 'text',
		                    'description' => __( 'Please enter your 2Checkout Private Key.', 'woocommerce' ),
		                    'default' => '',
		                    'desc_tip'      => true,
		                    'placeholder'	=> ''
					),
					
					'secret_word' => array(
							'title' => __( 'Secret Word', 'woocommerce' ),
		                    'type' 			=> 'text',
		                    'description' => __( 'Secret Word set under your 2Checkout -> My Account->Site Management.', 'woocommerce' ),
		                    'default' => '',
		                    'placeholder'	=> ''
					),
					'demo' => array(
							'title' => __( 'Enable Demo Mode', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Yes', 'woocommerce' ),
							'default' => 'yes'
					),
					'cc_form' => array(
							'title' => __( 'Credit Cart Form', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => __( 'Yes', 'woocommerce' ),
							'description' => __( 'It will show Credit Card Fields. Your site must have SSL Installed. Otherwise it will use regular checkout', 'woocommerce' ),
							'default' => 'yes',
							'desc_tip' => true,
					),
					'debug' => array(
                            'title'       => __( 'Debug Log', 'woocommerce' ),
                            'type'        => 'checkbox',
                            'label'       => __( 'Enable logging', 'woocommerce' ),
                            'default'     => 'no',
                            'description' => sprintf( __( 'Log 2Checkout events <em>%s</em>', 'woocommerce' ), wc_get_log_file_path( 'twocheckout' ) )
                        ),
                        
					'paypal_direct' => array(
							'title' => __( 'PayPal Direct', 'woocommerce' ),
							'type' => 'checkbox',
							'description' => __( 'As long as all of the required parameters are passed, the user will be redirected directly to PayPal to complete their payment.', 'woocommerce' ),
							'default'     => 'no',
							'desc_tip'      => true,
					),
					
					'xchange_rate' => array(
							'title' => __( 'Currency Converter', 'woocommerce' ),
							'type' => 'checkbox',
							'label' => twoco_currency_label($this->xchange_rate, $this->curr_converter_api),
							'default' => 'no',
							'description' => __( 'If your currency is not supported by 2CO, then use this option. It will automatically convert your currency to '.$this->converted_curr.' using API', 'woocommerce' ),
							'desc_tip'      => false,
					),
					'curr_converter_api' => array(
							'title' => __( 'Currency Converter API', 'woocommerce' ),
							'type' => 'text',
							'description' => __( 'Get API Key to use Currency Converter. <a target="_blank" href="'.esc_url($api_key_url).'">Get API Key.', 'woocommerce' ),
							'desc_tip'      => false,
					),
			);
		}
		
		public function payment_fields() {
			
			if ( $this->supports( 'tokenization' ) && is_checkout() ) {
				$this->tokenization_script();
				$this->saved_payment_methods();
				$this->form();
				$this->save_payment_method_checkbox();
			} else {
				$this->form();
			}
		}
		
		public function field_name( $name ) {
			return $this->supports( 'tokenization' ) ? '' : ' name="' . esc_attr( $this->id . '-' . $name ) . '" ';
		}
		
		public function form() {
			
			if ( $this->description ) {
				// you can instructions for test mode, I mean test card numbers etc.
				if ( $this->demo ) {
					$this->description .= ' TEST MODE ENABLED. In test mode, you can use the card numbers listed in <a href="#" target="_blank">documentation</a>.';
					$this->description  = trim( $this->description );
				}
				// display the description with <p> tags etc.
				echo wpautop( wp_kses_post( $this->description ) );
			}
			
			
			if ( ! $this->show_cc_form() ) {
				return;
			}
			
			wp_enqueue_script( 'wc-credit-card-form' );
			
	
			$fields = array();
	
			$cvc_field = '<p class="form-row form-row-last">
				<label for="' . esc_attr( $this->id ) . '-card-cvc">' . esc_html__( 'Card code', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label>
				<input id="' . esc_attr( $this->id ) . '-card-cvc" class="input-text wc-credit-card-form-card-cvc" inputmode="numeric" autocomplete="off" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" maxlength="4" placeholder="' . esc_attr__( 'CVC', 'woocommerce' ) . '" ' . $this->field_name( 'card-cvc' ) . ' style="width:100px" />
			</p>';
	
			$default_fields = array(
				'card-number-field' => '<p class="form-row form-row-wide">
					<label for="' . esc_attr( $this->id ) . '-card-number">' . esc_html__( 'Card number', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label>
					<input id="' . esc_attr( $this->id ) . '-card-number" class="input-text wc-credit-card-form-card-number" inputmode="numeric" autocomplete="cc-number" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="&bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull;" ' . $this->field_name( 'card-number' ) . ' />
				</p>',
				'card-expiry-field' => '<p class="form-row form-row-first">
					<label for="' . esc_attr( $this->id ) . '-card-expiry">' . esc_html__( 'Expiry (MM/YY)', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label>
					<input id="' . esc_attr( $this->id ) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" inputmode="numeric" autocomplete="cc-exp" autocorrect="no" autocapitalize="no" spellcheck="no" type="tel" placeholder="' . esc_attr__( 'MM / YY', 'woocommerce' ) . '" ' . $this->field_name( 'card-expiry' ) . ' />
				</p>',
			);
	
			if ( ! $this->supports( 'credit_card_form_cvc_on_saved_method' ) ) {
				$default_fields['card-cvc-field'] = $cvc_field;
			}
	
			$fields = wp_parse_args( $fields, apply_filters( 'woocommerce_credit_card_form_fields', $default_fields, $this->id ) );
			?>
	
			<fieldset id="wc-<?php echo esc_attr( $this->id ); ?>-cc-form" class='wc-credit-card-form wc-payment-form'>
				
				<input type="hidden" name="token" id="twoco_token">
				<?php do_action( 'woocommerce_credit_card_form_start', $this->id ); ?>
				<?php
				foreach ( $fields as $field ) {
					echo $field; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
				}
				?>
				<?php do_action( 'woocommerce_credit_card_form_end', $this->id ); ?>
				<div class="clear"></div>
			</fieldset>
			<?php
	
			if ( $this->supports( 'credit_card_form_cvc_on_saved_method' ) ) {
				echo '<fieldset>' . $cvc_field . '</fieldset>'; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped
			}
		}


		/**
		 * Process the payment and return the result
		 *
		 * @access public
		 * @param int $order_id
		 * @return array
		 */
		function process_payment( $order_id ) {


			// if no SSL found then use regular payment routine
			if ( ! $this->show_cc_form() ) {
				return $this->process_payment_regular( $order_id );
			}
			
			
			global $woocommerce;
			$order = new WC_Order( $order_id );
			
			$order_total = $this->get_price($order->get_total());
			
			if ( 'yes' == $this->debug ) {
                $this->log( 'Generating payment form for order ' . $order->get_order_number() );
			}
			
			$curr_code = get_woocommerce_currency();
			if( $this->convertion_is_on() ) {
				$curr_code = $this->converted_curr;
			}
			

			$old_wc = version_compare( WC_VERSION, '3.0', '<' );
            // 2Checkout Args
            $twocheckout_args = array(
                                    'token'         	=> $_POST['token'],
                                    'sellerId'      	=> $this->seller_id,
                                    'currency'			=> $curr_code,
                                    'total'         	=> $order_total,
                                    // Order key
                                    'merchantOrderId'	=> $old_wc ? $order->id : $order->get_order_number(),
                                    // Billing Address info
                                    "billingAddr" => array(
                                        'name'          => $old_wc ? $order->billing_first_name . ' ' . $order->billing_last_name : $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(),
                                        'addrLine1'     => $old_wc ? $order->billing_address_1 : $order->get_billing_address_1(),
                                        'addrLine2'     => $old_wc ? $order->billing_address_2 : $order->get_billing_address_2(),
                                        'city'          => $old_wc ? $order->billing_city : $order->get_billing_city(),
                                        'state'         => $old_wc ? $order->billing_state : $order->get_billing_state(),
                                        'zipCode'       => $old_wc ? $order->billing_postcode : $order->get_billing_postcode(),
                                        'country'       => $old_wc ? $order->billing_country : $order->get_billing_country(),
                                        'email'         => $old_wc ? $order->billing_email : $order->get_billing_email(),
                                        'phoneNumber'   => $old_wc ? $order->billing_phone : $order->get_billing_phone(),
                                    )
                                );
            
            if( $order -> needs_shipping_address() ) {
            	
            	$twocheckout_args['shippingAddr'] = array(
										            "name"		=> $old_wc ? $order->shipping_first_name . ' ' . $order->shipping_last_name : $order->get_shipping_first_name() . ' ' . $order->get_shipping_last_name(),
										            "addrLine1" => $old_wc ? $order->shipping_address_1 : $order->get_shipping_address_1(),
										            "city"		=> $old_wc ? $order->shipping_city : $order->get_shipping_city(),
										            "state" 	=> $old_wc ? $order->shipping_state : $order->get_shipping_state(),
										            "zipCode"	=> $old_wc ? $order->shipping_postcode : $order->get_shipping_postcode(),
										            "country"	=> $old_wc ? $order->shipping_country : $order->get_shipping_country(),
										            "email" 	=> '',
										            "phoneNumber" => ''
										        );
            }
                        
            Twocheckout::privateKey($this->private_key); 
			Twocheckout::sellerId($this->seller_id); 
            if ($this->demo) {
				Twocheckout::sandbox(true);
            }

			// ppom_pa($twocheckout_args); exit;

			try {
                  
                $charge = Twocheckout_Charge::auth($twocheckout_args);
                if (isset($charge['response']['responseCode']) && $charge['response']['responseCode'] == 'APPROVED') {
                	$order_number = $charge['response']['orderNumber'];
                	
                	$order->add_order_note( sprintf(__('Payment completed via 2Checkout Order Number %d', '2checkout'), $order_number) );
                    $order->payment_complete();
                    return array(
                        'result' => 'success',
                        'redirect' => $this->get_return_url( $order )
                    );
                }
            } catch (Twocheckout_Error $e) {
                wc_add_notice($e->getMessage(), $notice_type = 'error' );
                return;
            }         
            
		}

		/**
        * Logging method
        * @param  string $message
        */
        public static function log( $message ) {
            if ( self::$log_enabled ) {
                if ( empty( self::$log ) ) {
                    self::$log = new WC_Logger();
                }
                self::$log->add( 'twocheckout', $message );
            }
        }
        
        
        /**
		 * Process the payment and return the result
		 *
		 * @access public
		 * @param int $order_id
		 * @return array
		 */
		function process_payment_regular( $order_id ) {

			$order = new WC_Order( $order_id );
			
			// $this->demo = true;
			
			$twoco_args = $this->get_twoco_args( $order );
			// ppom_pa($twoco_args); exit;
			
			$twoco_args = http_build_query( $twoco_args, '', '&' );
			
			//if demo is enabled
			$checkout_url = '';
			if ($this->demo){
				$checkout_url =	$this->checkout_url_sandbox;
			}else{
				$checkout_url =	$this->checkout_url;
			}
			
			return array(
					'result' 	=> 'success',
					'redirect'	=> $checkout_url.'?'.$twoco_args
			);


		}
        
        
        /**
		 * Get 2Checkout Args for passing to PP
		 *
		 * @access public
		 * @param mixed $order
		 * @return array
		 */
		function get_twoco_args( $order ) {
			global $woocommerce;

			$order_id = $order->get_id();

			$curr_code = get_woocommerce_currency();
			if( $this->convertion_is_on() ) {
				$curr_code = $this->converted_curr;
			}
			
			// 2Checkout Args
			$twoco_args = array(
					'sid' 					=> $this->seller_id,
					'mode' 					=> '2CO',
					'merchant_order_id'		=> $order_id,
					'currency_code'			=> $curr_code,
						
					// Billing Address info
					'first_name'			=> $order->billing_first_name,
					'last_name'				=> $order->billing_last_name,
					'street_address'		=> $order->billing_address_1,
					'street_address2'		=> $order->billing_address_2,
					'city'					=> $order->billing_city,
					'state'					=> $order->billing_state,
					'zip'					=> $order->billing_postcode,
					'country'				=> $order->billing_country,
					'email'					=> $order->billing_email,
					'phone'					=> $order->billing_phone,
			);

			// Shipping
			
			if ($order->needs_shipping_address()) {

				$twoco_args['ship_name']			= $order->shipping_first_name.' '.$order->shipping_last_name;
				$twoco_args['company']				= $order->shipping_company;
				$twoco_args['ship_street_address']	= $order->shipping_address_1;
				$twoco_args['ship_street_address2']	= $order->shipping_address_2;
				$twoco_args['ship_city']			= $order->shipping_city;
				$twoco_args['ship_state']			= $order->shipping_state;
				$twoco_args['ship_zip']				= $order->shipping_postcode;
				$twoco_args['ship_country']			= $order->shipping_country;
			}
				
			
			$twoco_args['x_receipt_link_url'] 	= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'WC_Gateway_NM_TwoCheckout', home_url( '/' ) ) );
			$twoco_args['return_url']			= str_replace('https', 'http', $order->get_cancel_order_url());
			
			
			//setting purchas step
			// if all required filled it will skip other steps
			$twoco_args['purchase_step'] = 'payment-method';
			
			
			//if demo is enabled
			if ($this->demo){
				$twoco_args['demo'] =	'Y';
			}
			
			if( !$this->demo && $this->paypal_direct == 'yes' ) {
				$twoco_args['paypal_direct'] = 'Y';
			}

			$item_names = array();

			if ( sizeof( $order->get_items() ) > 0 ){
				
				$twoco_product_index = 0;
				
				foreach ( $order->get_items() as $item ){
					if ( $item['qty'] )
						$item_names[] = $item['name'] . ' x ' . $item['qty'];
				
					/**
					 * since version 1.6
					 * adding support for both WC Versions
					 */
					$_sku = '';
					if ( function_exists( 'get_product' ) ) {
							
						// Version 2.0
						$product = $order->get_product_from_item($item);
							
						// Get SKU or product id
						if ( $product->get_sku() ) {
							$_sku = $product->get_sku();
						} else {
							$_sku = $product->get_id();
						}
							
					} else {
							
						// Version 1.6.6
						$product = new WC_Product( $item['id'] );
							
						// Get SKU or product id
						if ( $product->get_sku() ) {
							$_sku = $product->get_sku();
						} else {
							$_sku = $item['id'];
						}	
					}
					
					$tangible = "N";
					
					$item_formatted_name 	= $item['name'] . ' (Product SKU: '.$item['product_id'].')';
				
					$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
					$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Order %s' , 'woocommerce'), $order->get_order_number() ) . " - " . $item_formatted_name;
					$twoco_args['li_'.$twoco_product_index.'_quantity'] = $item['qty'];
					$twoco_args['li_'.$twoco_product_index.'_price'] 	= $this->get_price($order->get_item_total( $item ));
					$twoco_args['li_'.$twoco_product_index.'_product_id'] = $_sku;
					$twoco_args['li_'.$twoco_product_index.'_tangible'] = $tangible;
					
					$twoco_product_index++;
				}
				
				//getting extra fees since version 2.0+
				$extrafee = $order -> get_fees();
				if($extrafee){
				
					
					$fee_index = 1;
					foreach ( $order -> get_fees() as $item ) {
						
						$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
						$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( 'Other Fee %s' , 'woocommerce'), $item['name'] );
						$twoco_args['li_'.$twoco_product_index.'_quantity'] = 1;
						$twoco_args['li_'.$twoco_product_index.'_price'] 	= $this->get_price( $item['line_total'] );

						$fee_index++;
						$twoco_product_index++;
	 				}	
				}
				
				// Shipping Cost
				if ( $order -> get_total_shipping() > 0 ) {
					
					
					$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'shipping';
					$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Shipping charges', 'woocommerce' );
					$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
					$twoco_args['li_'.$twoco_product_index.'_price'] 		= $this->get_price( $order -> get_total_shipping() );
					// $twoco_args['li_'.$twoco_product_index.'_tangible'] = 'Y';
					
					$twoco_product_index++;
				}
				
				// Taxes (shipping tax too)
				if ( $order -> get_total_tax() > 0 ) {
				
					$twoco_args['li_'.$twoco_product_index.'_type'] 		= 'tax';
					$twoco_args['li_'.$twoco_product_index.'_name'] 		= __( 'Tax', 'woocommerce' );
					$twoco_args['li_'.$twoco_product_index.'_quantity'] 	= 1;
					$twoco_args['li_'.$twoco_product_index.'_price'] 		= $this->get_price( $order->get_total_tax() );
					
					$twoco_product_index++;
				}

				
			}
			
			$twoco_args = apply_filters( 'woocommerce_twoco_args', $twoco_args );
			
			return $twoco_args;
		}

	
		/**
		 * Check for 2Checkout IPN Response
		 *
		 * @access public
		 * @return void
		 */
		function twocheckout_response() {
		
			/**
			 * source code: https://github.com/craigchristenson/woocommerce-2checkout-api
			 * Thanks to: https://github.com/craigchristenson
			 */
			global $woocommerce;
			
			@ob_clean();
			
			$wc_order_id 	= $_REQUEST['merchant_order_id'];
			
			$order_total = isset($_REQUEST['total']) ? $_REQUEST['total'] : '';
			
			
			if ( isset($_REQUEST['demo']) && $_REQUEST['demo'] == 'Y' ){
				$compare_string = $this->secret_word . $this->seller_id . "1" . $order_total;
			}else{
				$compare_string = $this->secret_word . $this->seller_id . $_REQUEST['order_number'] . $order_total;
			}
	
			if ($compare_hash1 != $compare_hash2) {
				wp_die( "2Checkout Hash Mismatch... check your secret word." );
			} else {
				$wc_order 	= new WC_Order( absint( $wc_order_id ) );
				$wc_order->add_order_note( sprintf(__('Payment completed via 2Checkout Order Number %d', '2checkout'), $_REQUEST['order_number']) );
				// Mark order complete
				$wc_order->payment_complete();
				// Empty cart and clear session
				$woocommerce->cart->empty_cart();
				wp_redirect( $this->get_return_url( $wc_order ) );
				exit;
			}
		}
		
		function get_price($price){
		
			if( $this->convertion_is_on() ){
				
				$price = twoco_get_converted_price( $price, $this->curr_converter_api );
			}
			
			$price = wc_format_decimal($price, wc_get_price_decimals());
			
			return apply_filters('xchanged_price', $price);
		}
		
		
		// Return T|F for credit card fields
		function show_cc_form() {
			
			$show_cc = true;
			
			if ( ! $this->demo && ! is_ssl() ) {
				$show_cc = false;
			}
			
			if( $this->cc_form != 'yes' ) {
				
				$show_cc = false;
			}
			
			return apply_filters('twoco_show_cc_form', $show_cc);
		}
		
		function convertion_is_on() {
		
			$enabled = false;
			if( $this->xchange_rate == 'yes' && $this->curr_converter_api != '' ) {
				$enabled = true;
			}
			
			return $enabled;
		}
	}
    // include TWOCO_PATH.'/lib/twoco-php/lib/Twocheckout.php';