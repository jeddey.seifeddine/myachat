<?php
/**
 * Direct Payment Button
 * @since 8.0
 * */
 
 class NM_DirectPay {
     
     // Logging
    public static $log_enabled = false;
    public static $log = false;
    
	var $seller_id;
	var $demo;
	var $plugin_url;

	public function __construct(){
	    
	   add_action('wp_enqueue_scripts', array($this, 'ajax_add_to_cart_js'));
	    
	    $twco_object            = new WC_Gateway_NM_TwoCheckout;
	    $this->seller_id        = $twco_object->get_option('seller_id');
		$this->demo 			= $twco_object->get_option('demo');
		$this->debug 			= $twco_object->get_option('debug');
		$this->checkout_url     = 'https://www.2checkout.com/checkout/purchase';
		$this->checkout_url_sandbox	= 'https://sandbox.2checkout.com/checkout/purchase';
		$this->direct_pay_title = 'Pay'; //$twco_object->get_option('direct_pay_title');
		$this->inline			= $twco_object->inline_checkout_on();
		
		$this->demo				= $this->demo == 'yes' ? true : false;
		
	    $this->direct_payment	= true;
			
		self::$log_enabled = $this->debug;
		
		// Direcy pay button
		add_filter('twoco_order_no_received', array($this, 'generate_wc_order'), 10, 2);
		
		add_action('wp_ajax_woocommerce_ajax_add_to_cart', array($this, 'woocommerce_ajax_add_to_cart') );
        add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', array($this,'woocommerce_ajax_add_to_cart') );
        
        // Adding inline form html in cart fragment
        // add the filter 
        // add_filter( 'woocommerce_add_to_cart_fragments', array($this, 'add_to_cart_fragments'), 10, 1 ); 
        
        // Placehold for inline form
        add_action('woocommerce_after_add_to_cart_form', array($this, 'render_inline_form'));
	    
	}
	
	
	/**
    * Logging method
    * @param  string $message
    */
    public static function log( $message ) {
        if ( self::$log_enabled ) {
            if ( empty( self::$log ) ) {
                self::$log = new WC_Logger();
            }
            
            $message = is_array($message) ? json_encode($message) : $message;
            self::$log->add( 'twocheckout', $message );
        }
    }
    
    // Generating 2Checkout order
    function twoco_generate_wc_order($order, $request_data) {
    	
    	if (strpos($order, 'PRODUCT') === false) return $order;
    	
    	// Getting Product ID
    	$product_id = substr($order, 8);
    	
    	$defaults = array(
    		'type' => 'post',
    		'before' => "<p>",
    		'after' => "</p> \n",
    		'echo' => TRUE
    	);
    	
    	$defaults = array(
          'first_name' => 'John',
          'last_name'  => 'Conlin',
          'company'    => 'Speed Society',
          'email'      => 'joe@testing.com',
          'phone'      => '760-555-1212',
          'address_1'  => '123 Main st.',
          'address_2'  => '104',
          'city'       => 'San Diego',
          'state'      => 'Ca',
          'postcode'   => '92121',
          'country'    => 'US'
      );
    	
	  $address = wp_parse_args( $request_data, $defaults );
    	
    	// Now we create the order
      $order = wc_create_order();
    
      $product = wc_get_product($product_id);
      $order->add_product( $product, 1); // This is an existing SIMPLE product
      $order->set_address( $address, 'billing' );
      //
      $order->calculate_totals();
      $order->add_order_note( sprintf(__('Direct Payment Received for Product: %s', '2checkout'), $product->get_title()) );
      $order->update_status('pending', 'order_note');
      
      $order_id = $order->get_id();
      
    	return $order_id;
    }
    
    
        
    function woocommerce_ajax_add_to_cart() {
    
        // Resetting cart
        WC()->cart->empty_cart(); 
        // wp_send_json($_POST);
        $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
        $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
        $variation_id = absint($_POST['variation_id']);
        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
        $product_status = get_post_status($product_id);

        if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

            do_action('woocommerce_ajax_added_to_cart', $product_id);

            if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                wc_add_to_cart_message(array($product_id => $quantity), true);
            }

            WC_AJAX :: get_refreshed_fragments();
        } else {

            $data = array(
                'error' => true,
                'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

            echo wp_send_json($data);
        }

        wp_die();
    }
	
	
	function ajax_add_to_cart_js() {
	    
	    if( $this->inline ) {
	        // wp_enqueue_script( 'twoco-inline','//www.2checkout.com/static/checkout/javascript/direct.min.js' );
	    }
	    
	    if (function_exists('is_product') && is_product() && $this->direct_payment) {
	        wp_enqueue_script('woocommerce-ajax-add-to-cart', TWOCO_URL.'/js/ajax-add-to-cart.js', array('jquery'), '', true);
	    }
	}
	
	// define the woocommerce_add_to_cart_fragments callback 
    function add_to_cart_fragments( $cart_fragment ) { 
        
        
        // var_dump($checkout_url);
		
		$twoco_args = $this->get_twoco_args();
		
		if( !$twoco_args ) return $cart_fragment;
		
        $inline_form = "";
				
		
		foreach($twoco_args as $key => $val){
			
			if($key === 'x_receipt_link_url' || $key === 'return_url'){
				$val = esc_url($val);
			}else{
				$val = esc_attr($val);
			}
			
			$inline_form .= "<input type='hidden' name='".esc_attr($key)."' value='".$val."' />";
		}
		
		$inline_form .= "<input id='btn-inline-form' name='submit' class='button alt' type='submit' value='".esc_attr($this->direct_pay_title)."' />";
		
// 		$inline_form .= "</form>";
		
		$cart_fragment['twoco_form_data'] = $inline_form;
		
        return $cart_fragment; 
    }
	
	function render_inline_form() {
	    
	    $checkout_url = '';
        if ($this -> demo){
			$checkout_url =	$this->checkout_url_sandbox;
		}else{
			$checkout_url =	$this->checkout_url;
		}
		
		$twoco_args = $this->get_twoco_args();
        // $inline_form = '<form id="twoco-inline-form" class="twoco-direct-pay-container" action="'.esc_url($checkout_url).'" method="post">';
        $inline_form = '<form action="'.esc_url($checkout_url).'" method="post">';
				
		foreach($twoco_args as $key => $val){
			
			if($key === 'x_receipt_link_url' || $key === 'return_url'){
				$val = esc_url($val);
			}else{
				$val = esc_attr($val);
			}
			
			$inline_form .= "<input type='hidden' name='".esc_attr($key)."' value='".$val."' />";
		}
		
		$inline_form .= $this->render_address_fields();
		
		$inline_form .= "<input class='button alt' type='submit' value='".esc_attr($this->direct_pay_title)."' />";
		$inline_form .= "</form>";
		
		echo $inline_form;
		
		// wp_enqueue_script( 'twoco-inline','//www.2checkout.com/static/checkout/javascript/direct.min.js', array( 'jquery' ),'',true );
	}
	
	/**
	 * Get 2Checkout Args for passing to PP
	 *
	 * @access public
	 * @param mixed $order
	 * @return array
	 */
	function get_twoco_args() {
		
		
		global $product;
		
		/*foreach( WC()->cart->get_cart() as $cart_item ) {
		    
		    $product = $cart_item['data'];
		}*/
		
		$order_id = $product->get_id();

		$curr_code = get_woocommerce_currency();
		/*if( $this->convertion_is_on() ) {
			$curr_code = $this->converted_curr;
		}*/
		
		// 2Checkout Args
		$twoco_args = array(
				'sid' 					=> $this->seller_id,
				'mode' 					=> '2CO',
				'merchant_order_id'		=> $order_id,
				'currency_code'			=> $curr_code,
					
				/*// Billing Address info
				'first_name'			=> $order->get_billing_first_name(),
				'last_name'				=> $order->get_billing_last_name(),
				'street_address'		=> $order->get_billing_address_1(),
				'street_address2'		=> $order->get_billing_address_2(),
				'city'					=> $order->get_billing_city(),
				'state'					=> $order->get_billing_state(),
				'zip'					=> $order->get_billing_postcode(),
				'country'				=> $order->get_billing_country(),
				'email'					=> $order->get_billing_email(),
				'phone'					=> $order->get_billing_phone(),*/
		);

		$twoco_args['x_receipt_link_url'] 	= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'wc_gateway_nm_twocheckout', home_url( '/' ) ) );
		$twoco_args['return_url']			= str_replace('https', 'http', get_permalink($product->get_id()));
		
		
		//if demo is enabled
		if ($this->demo){
			$twoco_args['demo'] =	'Y';
		}
		
		$twoco_product_index = 0;
		$tangible = "N";
		$twoco_args['li_'.$twoco_product_index.'_type'] 	= 'product';
		$twoco_args['li_'.$twoco_product_index.'_name'] 	= sprintf( __( '%s' , 'woocommerce'), $product->get_title() );
		$twoco_args['li_'.$twoco_product_index.'_quantity'] = 1;
		$twoco_args['li_'.$twoco_product_index.'_price'] 	= $product->get_price();
		$twoco_args['li_'.$twoco_product_index.'_product_id'] = $product->get_id();
		$twoco_args['li_'.$twoco_product_index.'_tangible'] = $tangible;
		
		$twoco_args = apply_filters( 'woocommerce_twoco_args', $twoco_args );
		
		return $twoco_args;
	}
	
	// Render Billing/Address Info fields
	function render_address_fields() {
		
		$address_fields = '';
		
		ob_start();
			woocommerce_form_field( 'card_holder_name', array(
				'type'        => 'text',
				'required'    => true,
				'label'       => 'Full Name',
				'description' => 'Please enter your Fullname',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'email', array(
				'type'        => 'email',
				'required'    => true,
				'label'       => 'Email',
				'description' => 'Please enter your Email',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'street_address', array(
				'type'        => 'text',
				'required'    => true,
				'label'       => 'Address 1',
				'description' => 'Address 1',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'street_address2', array(
				'type'        => 'text',
				'required'    => true,
				'label'       => 'Address 2',
				'description' => 'Address 2',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'city', array(
				'type'        => 'text',
				'required'    => true,
				'label'       => 'City',
				'description' => 'Please enter your City',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'country', array(
				'type'        => 'country',
				'required'    => true,
				'label'       => 'Country',
				'description' => 'Please select your Country',
			));
		$address_fields .= ob_get_clean();
		
		ob_start();
			woocommerce_form_field( 'state', array(
				'type'        => 'state',
				'required'    => true,
				'label'       => 'State',
				'description' => 'Please enter your State',
			));
		$address_fields .= ob_get_clean();
		
		return $address_fields;
	}
		
 }
 
 new NM_DirectPay;