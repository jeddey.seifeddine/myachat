<?php

/* * class
 * Description of WCVS_SwatchesPlugin
 *
 * @author ma_group
 * 
 * @include_action: plugins_loaded
 * 
 * @position: 100
 */

class WCVS_SwatchesPlugin {

    private $product_attribute_images;
    private $product_swatches_render;

    public function __construct() {
        add_action('init', array(&$this, 'on_init'));

        add_action('wc_quick_view_enqueue_scripts', array($this, 'on_enqueue_scripts'));
        add_action('wp_enqueue_scripts', array(&$this, 'on_enqueue_scripts'));


        add_action('admin_head', array(&$this, 'on_enqueue_scripts'));

        $this->product_attribute_images = new WCVS_Swatches_Product_Attribute_Images('swatches_id', 'swatches_image_size');
        $this->product_data_tab = new WCVS_Swatches_Product_Data_Tab();
        $this->product_swatches_render = new WCVS_Swatches_Render();

        //Swatch Image Size Settings
        add_filter('woocommerce_catalog_settings', array(&$this, 'swatches_image_size_setting')); // pre WC 2.1
        add_filter('woocommerce_product_settings', array(&$this, 'swatches_image_size_setting')); // WC 2.1+
        add_filter('woocommerce_get_image_size_swatches', array($this, 'get_image_size_swatches'));

        add_filter('woocommerce_dropdown_variation_attribute_options_html', array($this, 'render_variation_attribute'), 20, 2);
    }

    public function on_init() {
        global $woocommerce;
        $image_size = get_option('swatches_image_size', array());
        $size = array();

        $size['width'] = isset($image_size['width']) && !empty($image_size['width']) ? $image_size['width'] : '32';
        $size['height'] = isset($image_size['height']) && !empty($image_size['height']) ? $image_size['height'] : '32';
        $size['crop'] = isset($image_size['crop']) ? $image_size['crop'] : 1;

        $image_size = apply_filters('woocommerce_get_image_size_swatches_image_size', $size);

        add_image_size('swatches_image_size', apply_filters('woocommerce_swatches_size_width_default', $image_size['width']), apply_filters('woocommerce_swatches_size_height_default', $image_size['height']), $image_size['crop']);
    }

    public function render_variation_attribute($html, $args) {
        $this->product_swatches_render->render($html, $args);
    }

    public function on_enqueue_scripts() {
        global $pagenow;

        if (!is_admin()) {
            wp_enqueue_style('swatches', WCVS_ROOT_URL . '/assets/css/swatches.css', array(), WCVS_SWATCHES_VERSION);
            wp_enqueue_style('swatches-twentytwelve', WCVS_ROOT_URL . '/assets/css/swatches-twenty-twelve.css', array(), WCVS_SWATCHES_VERSION);
            wp_enqueue_style('swatches-twentynineteen', WCVS_ROOT_URL . '/assets/css/swatches-twenty-nineteen.css', array(), WCVS_SWATCHES_VERSION);
            wp_enqueue_script('swatches', WCVS_ROOT_URL . '/assets/js/swatches.js', array('jquery'), WCVS_SWATCHES_VERSION, true);

            $data = array(
                'ajax_url' => admin_url('admin-ajax.php')
            );

            wp_localize_script('swatches', 'wc_swatches_params', $data);
        }

        if (is_admin() && ( $pagenow == 'post-new.php' || $pagenow == 'post.php' || $pagenow == 'edit.php' || $pagenow == 'edit-tags.php' )) {
            wp_enqueue_media();
            wp_enqueue_style('swatches', WCVS_ROOT_URL . '/assets/css/swatches.css');
            wp_enqueue_style('swatches-twentytwelve', WCVS_ROOT_URL . '/assets/css/swatches-twenty-twelve.css', array(), WCVS_SWATCHES_VERSION);
            wp_enqueue_style('swatches-twentynineteen', WCVS_ROOT_URL . '/assets/css/swatches-twenty-nineteen.css', array(), WCVS_SWATCHES_VERSION);
            wp_enqueue_script('swatches-admin', WCVS_ROOT_URL . '/assets/js/swatches-admin.js', array('jquery'), '1.0', true);

            wp_enqueue_style('colourpicker', WCVS_ROOT_URL . '/assets/css/colorpicker.css');
            wp_enqueue_script('colourpicker', WCVS_ROOT_URL . '/assets/js/colorpicker.js', array('jquery'));


            $data = array(
                'placeholder_img_src' => apply_filters('woocommerce_placeholder_img_src', WCVS_ROOT_URL . '/assets/images/placeholder.png')
            );

            wp_localize_script('swatches-admin', 'wc_swatches_params', $data);
        }
    }

    public function plugin_url() {
        return untrailingslashit(plugin_dir_url(__FILE__));
    }

    public function plugin_dir() {
        return plugin_dir_path(__FILE__);
    }

    public function swatches_image_size_setting($settings) {
        $setting = array(
            'name' => __('Swatches', 'wcvs_swatches'),
            'desc' => __('The default size for color swatches and photos.', 'wcvs_swatches'),
            'id' => 'swatches_image_size',
            'css' => '',
            'type' => 'image_width',
            'std' => '32',
            'desc_tip' => true,
            'default' => array(
                'crop' => 1,
                'width' => 32,
                'height' => 32
            )
        );

        $index = count($settings) - 1;

        $settings[$index + 1] = $settings[$index];
        $settings[$index] = $setting;

        return $settings;
    }

    public function get_image_size_swatches($size) {
        $image_size = get_option('swatches_image_size', array());
        $size = array();

        $size['width'] = isset($image_size['width']) && !empty($image_size['width']) ? $image_size['width'] : '32';
        $size['height'] = isset($image_size['height']) && !empty($image_size['height']) ? $image_size['height'] : '32';
        $size['crop'] = isset($image_size['crop']) && $image_size['crop'] ? 1 : 0;

        $image_size = apply_filters('woocommerce_get_image_size_swatches_image_size', $size);

        //Need to remove the filter because woocommerce will disable the input field.
        remove_filter('woocommerce_get_image_size_swatches', array($this, 'get_image_size_swatches'));

        return $image_size;
    }

}
