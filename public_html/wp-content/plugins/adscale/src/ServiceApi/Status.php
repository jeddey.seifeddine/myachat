<?php

namespace AdScale\ServiceApi;

use AdScale\Handlers\Status as StatusHandler;
use AdScale\Helpers\Helper;

class Status extends ServiceApiBase {
	
	public static function initContent() {
		global $wp;
		
		if ( isset( $_GET['adscaleStatus'] ) ) {
			$wp->query_vars['adscaleStatus'] = sanitize_key( wp_unslash( $_GET['adscaleStatus'] ) );
		}
		
		//  make sure endpoint request
		if ( ! isset( $wp->query_vars['adscaleStatus'] ) ) {
			return;
		}
		
		
		try {
			ob_start();
			
			Helper::nocacheHeaders();
			
			// Check Request (Guards)
			self::checkRequest();
			
			ob_end_clean();
			
			// Handle Request
			self::handleRequest();
			
			// Default error
			Helper::sendResponseFormattedError( self::makeErrorBody() );
		} catch ( ServiceApiExeption $ex ) {
			Helper::sendResponseFormattedError(
				self::makeErrorBody( $ex->getApiErrorCode(), $ex->getMessage() )
			);
		} catch ( \Exception $ex ) {
			Helper::sendResponseFormattedError( self::makeErrorBody( $ex->getCode(), $ex->getMessage() ) );
		}
	}
	
	
	
	public static function handleRequest() {
		$statusData = StatusHandler::getStatusData();
		
		$is_format_json = strtoupper( Helper::getPostBodyDataValue( 'format' ) ) === 'JSON';
		$is_format_json = $is_format_json
			? $is_format_json
			: ! empty( $_GET['format'] ) && strtoupper( $_GET['format'] ) === 'JSON';
		
		
		if ( $is_format_json ) {
			$response = StatusHandler::renderDataToJson( $statusData );
			Helper::sendResponse( Helper::formatResponse( $response ), 200, '', 'text/plain' );
		} else {
			$response = StatusHandler::renderDataToHtml( $statusData );
			Helper::sendResponse( Helper::formatResponse( $response ), 200, '', 'text/html' );
		}
	}
}
