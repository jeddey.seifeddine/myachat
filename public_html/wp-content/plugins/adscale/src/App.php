<?php

namespace AdScale;

use AdScale\Handlers\ApiHandler;
use AdScale\Handlers\PluginActivation;
use AdScale\Handlers\Settings;
use AdScale\Handlers\MetaTags;
use AdScale\Handlers\Assets;
use AdScale\Handlers\WooHandlers;
use AdScale\Handlers\Heartbeat;
use AdScale\Handlers\Cart;
use AdScale\Helpers\Logger;

final class App extends AbstractSingleton {
	
	private $config;
	
	
	protected function __construct() {
		parent::__construct();
	}
	
	
	
	
	public function getConfig() {
		return $this->config;
	}
	
	
	
	
	public function run( $config ) {
		
		if ( ! $this->is_woocommerce_activated() ) {
			return;
		}
		
		$this->config = $config;
		Logger::setConfig( ! empty( $this->config['logger'] ) ? $this->config['logger'] : [] );
		$this->run_hooks();
	}
	
	
	
	
	public function run_hooks() {
		
		register_activation_hook( ADSCALE_PLUGIN_FILE, [ PluginActivation::class, 'activateHandler' ] );
		register_deactivation_hook( ADSCALE_PLUGIN_FILE, [ PluginActivation::class, 'deactivateHandler' ] );
		
		// Settings menu
		add_action( 'admin_menu', [ Settings::class, 'menu' ], 777 );
		add_action( 'admin_init', [ Settings::class, 'settings_init' ], 777 );
		
		// meta tag google-site-verification
		add_action( 'wp_head', [ MetaTags::class, 'put_google_site_verification_tag' ], 777 );
		
		// load assets
		add_action( 'wp_enqueue_scripts', [ Assets::class, 'load_assets' ], 777 );
		add_action( 'admin_enqueue_scripts', [ Assets::class, 'load_admin_assets' ], 777 );
		
		// Redirect after plugin activation
		add_action( 'admin_init', [ PluginActivation::class, 'redirectAfterActivation' ], 777 );
		
		// add data-adscale-product_price
		add_filter( 'woocommerce_loop_add_to_cart_args', [ WooHandlers::class, 'change_add_to_cart_args' ], 777, 2 );
		
		// add js script with trigger function from adscale script
		add_action( 'woocommerce_add_to_cart', [ WooHandlers::class, 'trigger_after_add_to_cart_event' ], 777, 6 );
		
		// Add functionality on Order creation
		add_action( 'woocommerce_checkout_order_processed', [ WooHandlers::class, 'after_checkout_order_processed' ], 777, 3 );
		
		// Add cookie for Order after status changed to needed
		add_action( 'woocommerce_order_status_processing', [ WooHandlers::class, 'order_status' ], 777, 2 );
		add_action( 'woocommerce_order_status_completed', [ WooHandlers::class, 'order_status' ], 777, 2 );
		add_action( 'woocommerce_order_status_on-hold', [ WooHandlers::class, 'order_status' ], 777, 2 );
		
		// heartbeat
		add_action( 'init', [ Heartbeat::class, 'activate' ] );
		
		// Api
		add_action( 'init', [ ApiHandler::class, 'addEndpoints' ], 0 );
		add_action( 'parse_request', [ ApiHandler::class, 'handleApiRequests' ], 0 );
		add_filter( 'query_vars', [ ApiHandler::class, 'addQueryVars' ], 0 );
		
		// ajax: adscale/ajaxGetCart 
		add_action( 'wp_ajax_adscale/ajaxGetCart', [ Cart::class, 'ajaxGetCart' ] );
		add_action( 'wp_ajax_nopriv_adscale/ajaxGetCart', [ Cart::class, 'ajaxGetCart' ] );
	}
	
	
	
	
	public function is_woocommerce_activated() {
		$blog_plugins = get_option( 'active_plugins', [] );
		$site_plugins = is_multisite() ? (array) maybe_unserialize( get_site_option( 'active_sitewide_plugins' ) ) : [];
		
		return in_array( 'woocommerce/woocommerce.php', $blog_plugins, true ) || isset( $site_plugins['woocommerce/woocommerce.php'] );
	}
}