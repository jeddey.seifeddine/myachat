<?php
define('WP_MEMORY_LIMIT', '512M');
define('WP_HOME','https://myachat.com');
define('WP_SITEURL','https://myachat.com');
/** SSL */  
define('FORCE_SSL_ADMIN', true);  
// in some setups HTTP_X_FORWARDED_PROTO might contain  
// a comma-separated list e.g. http,https  
// so check for https existence  
if (strpos($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)  
    $_SERVER['HTTPS']='on';

if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $location);
    exit;
}
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/home/myachat/public_html/wp-content/plugins/wp-super-cache/' );
define( 'WP_DEBUG', false );
define( 'DB_NAME', 'ecommerce' );

/** MySQL database username */
define( 'DB_USER', 'perfotech' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Edin123!' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dL2JqsO6RCdlIYF/zU}SmoqiRne4_7mgVzJw3Gmy5Sn`}*hW:~{4#]$`Jd`^0p>D' );
define( 'SECURE_AUTH_KEY',  'Un_j0GN{:NV!A~b|v+ALyYN)Q]I[z$B:!&k!K8(xV?5./]]6BBmHGQD~h^5HX.Z3' );
define( 'LOGGED_IN_KEY',    '8mq;dC_Aq!k(Y5ud]hS&CJ>tlX^[c<[m(L=3~dr`O~>48f7:{<3aMOJ//D &yoG*' );
define( 'NONCE_KEY',        'sI3jl [~h1^h006KtC,A_b&@p2UO$5~&g=}#Y;Fjy&Cfc?3B/<aSUQ/-`SFY 8IH' );
define( 'AUTH_SALT',        '%h+LuUd!*Nw*3I9AOl Qe+0FF;sgq%QA[Y,obRBvSW]f6[;-khA},$gSq98H,AU*' );
define( 'SECURE_AUTH_SALT', '_?YarUAXq#(}VSni)RhtnIiX%o]sFw_-KS7;/ckX+o@ZoF.xoF&y|[k|^aV6E^m#' );
define( 'LOGGED_IN_SALT',   '=`@-BK_6xh{}4ZuRp:GpaPhfEyC-e$|Ah3j1h7>fKpQVlgN/p%37PDH./i3YNKY.' );
define( 'NONCE_SALT',       'F($O90P?[3f icJhOplRuj0#|9v=WN:Ze5F]:1;Q^VK5Bu*~|uW`KIg%yW(U>*MZ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
